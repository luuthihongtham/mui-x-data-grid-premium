import { useGridApiRef as useCommunityGridApiRef } from '@mui/x-data-grid-pro';
export const useGridApiRef = useCommunityGridApiRef;