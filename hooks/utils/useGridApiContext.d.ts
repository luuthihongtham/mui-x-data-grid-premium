import * as React from 'react';
import { GridApiCommon } from '@mui/x-data-grid-pro';
import { GridApiPremium } from '../../models/gridApiPremium';
export declare const useGridApiContext: <GridApi extends GridApiCommon = GridApiPremium>() => React.MutableRefObject<GridApi>;
