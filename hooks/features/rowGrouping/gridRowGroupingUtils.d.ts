import * as React from 'react';
import { GridRowTreeConfig, GridFilterState, GridFilterModel } from '@mui/x-data-grid-pro';
import { GridAggregatedFilterItemApplier, GridColumnRawLookup, GridApiCommunity } from '@mui/x-data-grid-pro/internals';
import { DataGridPremiumProcessedProps } from '../../../models/dataGridPremiumProps';
import { GridGroupingRules, GridRowGroupingModel } from './gridRowGroupingInterfaces';
import { GridStatePremium } from '../../../models/gridStatePremium';
import { GridApiPremium } from '../../../models/gridApiPremium';
export declare const GRID_ROW_GROUPING_SINGLE_GROUPING_FIELD = "__row_group_by_columns_group__";
export declare const ROW_GROUPING_STRATEGY = "grouping-columns";
export declare const getRowGroupingFieldFromGroupingCriteria: (groupingCriteria: string | null) => string;
export declare const getRowGroupingCriteriaFromGroupingField: (groupingColDefField: string) => string | null;
export declare const isGroupingColumn: (field: string) => boolean;
interface FilterRowTreeFromTreeDataParams {
    rowTree: GridRowTreeConfig;
    isRowMatchingFilters: GridAggregatedFilterItemApplier | null;
    filterModel: GridFilterModel;
    apiRef: React.MutableRefObject<GridApiCommunity>;
}
/**
 * A leaf is visible if it passed the filter
 * A group is visible if all the following criteria are met:
 * - One of its children is passing the filter
 * - It is passing the filter
 */
export declare const filterRowTreeFromGroupingColumns: (params: FilterRowTreeFromTreeDataParams) => Omit<GridFilterState, 'filterModel'>;
export declare const getColDefOverrides: (groupingColDefProp: DataGridPremiumProcessedProps['groupingColDef'], fields: string[]) => import("@mui/x-data-grid-pro").GridGroupingColDefOverride<any> | null | undefined;
export declare const mergeStateWithRowGroupingModel: (rowGroupingModel: GridRowGroupingModel) => (state: GridStatePremium) => GridStatePremium;
export declare const setStrategyAvailability: (apiRef: React.MutableRefObject<GridApiPremium>, disableRowGrouping: boolean) => void;
export declare const getGroupingRules: ({ sanitizedRowGroupingModel, columnsLookup, }: {
    sanitizedRowGroupingModel: GridRowGroupingModel;
    columnsLookup: GridColumnRawLookup;
}) => GridGroupingRules<import("@mui/x-data-grid-pro").GridValidRowModel>;
/**
 * Compares two sets of grouping rules to determine if they are equal or not.
 */
export declare const areGroupingRulesEqual: (previousValue: GridGroupingRules | undefined, newValue: GridGroupingRules<import("@mui/x-data-grid-pro").GridValidRowModel>) => boolean;
export {};
