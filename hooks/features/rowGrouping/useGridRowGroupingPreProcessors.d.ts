import * as React from 'react';
import { DataGridPremiumProcessedProps } from '../../../models/dataGridPremiumProps';
import { GridApiPremium } from '../../../models/gridApiPremium';
export declare const useGridRowGroupingPreProcessors: (apiRef: React.MutableRefObject<GridApiPremium>, props: Pick<DataGridPremiumProcessedProps, 'disableRowGrouping' | 'groupingColDef' | 'rowGroupingColumnMode' | 'defaultGroupingExpansionDepth' | 'isGroupExpandedByDefault'>) => void;
