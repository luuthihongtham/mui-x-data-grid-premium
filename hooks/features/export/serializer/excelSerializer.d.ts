import type * as Excel from 'exceljs';
import { GridStateColDef, GridRowId, GridApi } from '@mui/x-data-grid-pro';
import { GridExceljsProcessInput, ColumnsStylesInterface } from '../gridExcelExportInterface';
interface BuildExcelOptions {
    columns: GridStateColDef[];
    rowIds: GridRowId[];
    includeHeaders: boolean;
    includeColumnGroupsHeaders: boolean;
    valueOptionsSheetName: string;
    exceljsPreProcess?: (processInput: GridExceljsProcessInput) => Promise<void>;
    exceljsPostProcess?: (processInput: GridExceljsProcessInput) => Promise<void>;
    columnsStyles?: ColumnsStylesInterface;
}
export declare function buildExcel(options: BuildExcelOptions, api: GridApi): Promise<Excel.Workbook>;
export {};
