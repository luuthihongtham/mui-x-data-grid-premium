import * as React from 'react';
import { useGridApiMethod, useGridLogger } from '@mui/x-data-grid';
import { useGridRegisterPipeProcessor, exportAs, getColumnsToExport, defaultGetRowsToExport } from '@mui/x-data-grid/internals';
import { buildExcel } from './serializer/excelSerializer';
import { GridExcelExportMenuItem } from '../../../components';
/**
 * @requires useGridColumns (state)
 * @requires useGridFilter (state)
 * @requires useGridSorting (state)
 * @requires useGridSelection (state)
 * @requires useGridParamsApi (method)
 */

import { jsx as _jsx } from "react/jsx-runtime";
export const useGridExcelExport = apiRef => {
  const logger = useGridLogger(apiRef, 'useGridExcelExport');
  const getDataAsExcel = React.useCallback((options = {}) => {
    var _options$getRowsToExp, _options$includeHeade, _options$includeColum;

    logger.debug(`Get data as excel`);
    const getRowsToExport = (_options$getRowsToExp = options.getRowsToExport) != null ? _options$getRowsToExp : defaultGetRowsToExport;
    const exportedRowIds = getRowsToExport({
      apiRef
    });
    const exportedColumns = getColumnsToExport({
      apiRef,
      options
    });
    return buildExcel({
      columns: exportedColumns,
      rowIds: exportedRowIds,
      includeHeaders: (_options$includeHeade = options.includeHeaders) != null ? _options$includeHeade : true,
      includeColumnGroupsHeaders: (_options$includeColum = options.includeColumnGroupsHeaders) != null ? _options$includeColum : true,
      valueOptionsSheetName: (options == null ? void 0 : options.valueOptionsSheetName) || 'Options',
      columnsStyles: options == null ? void 0 : options.columnsStyles,
      exceljsPreProcess: options == null ? void 0 : options.exceljsPreProcess,
      exceljsPostProcess: options == null ? void 0 : options.exceljsPostProcess
    }, apiRef.current);
  }, [logger, apiRef]);
  const exportDataAsExcel = React.useCallback(async options => {
    logger.debug(`Export data as excel`);
    const workbook = await getDataAsExcel(options);

    if (workbook === null) {
      return;
    }

    const content = await workbook.xlsx.writeBuffer();
    const blob = new Blob([content], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    });
    exportAs(blob, 'xlsx', options == null ? void 0 : options.fileName);
  }, [logger, getDataAsExcel]);
  const excelExportApi = {
    getDataAsExcel,
    exportDataAsExcel
  };
  useGridApiMethod(apiRef, excelExportApi, 'GridExcelExportApi');
  /**
   * PRE-PROCESSING
   */

  const addExportMenuButtons = React.useCallback((initialValue, options) => {
    var _options$excelOptions;

    if ((_options$excelOptions = options.excelOptions) != null && _options$excelOptions.disableToolbarButton) {
      return initialValue;
    }

    return [...initialValue, {
      component: /*#__PURE__*/_jsx(GridExcelExportMenuItem, {
        options: options.excelOptions
      }),
      componentName: 'excelExport'
    }];
  }, []);
  useGridRegisterPipeProcessor(apiRef, 'exportMenu', addExportMenuButtons);
};