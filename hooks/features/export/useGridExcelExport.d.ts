import * as React from 'react';
import { GridApiPremium } from '../../../models/gridApiPremium';
/**
 * @requires useGridColumns (state)
 * @requires useGridFilter (state)
 * @requires useGridSorting (state)
 * @requires useGridSelection (state)
 * @requires useGridParamsApi (method)
 */
export declare const useGridExcelExport: (apiRef: React.MutableRefObject<GridApiPremium>) => void;
