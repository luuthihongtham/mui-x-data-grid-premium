import * as React from 'react';
import { GridApiPremium } from '../../../models/gridApiPremium';
import { DataGridPremiumProcessedProps } from '../../../models/dataGridPremiumProps';
export declare const useGridAggregationPreProcessors: (apiRef: React.MutableRefObject<GridApiPremium>, props: Pick<DataGridPremiumProcessedProps, 'aggregationFunctions' | 'disableAggregation' | 'getAggregationPosition'>) => void;
