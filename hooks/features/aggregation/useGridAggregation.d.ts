import * as React from 'react';
import { GridStateInitializer } from '@mui/x-data-grid-pro/internals';
import { DataGridPremiumProcessedProps } from '../../../models/dataGridPremiumProps';
import { GridApiPremium } from '../../../models/gridApiPremium';
export declare const aggregationStateInitializer: GridStateInitializer<Pick<DataGridPremiumProcessedProps, 'aggregationModel' | 'initialState'>, GridApiPremium>;
export declare const useGridAggregation: (apiRef: React.MutableRefObject<GridApiPremium>, props: Pick<DataGridPremiumProcessedProps, 'onAggregationModelChange' | 'initialState' | 'aggregationModel' | 'getAggregationPosition' | 'aggregationFunctions' | 'aggregationRowsScope' | 'disableAggregation' | 'rowGroupingColumnMode'>) => void;
