import { GridStatePremium } from '../../../models/gridStatePremium';
export declare const gridAggregationStateSelector: (state: GridStatePremium) => import("./gridAggregationInterfaces").GridAggregationState;
export declare const gridAggregationModelSelector: import("@mui/x-data-grid").OutputSelector<GridStatePremium, import("./gridAggregationInterfaces").GridAggregationModel>;
export declare const gridAggregationLookupSelector: import("@mui/x-data-grid").OutputSelector<GridStatePremium, import("./gridAggregationInterfaces").GridAggregationLookup>;
