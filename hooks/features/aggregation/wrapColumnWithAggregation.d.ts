import * as React from 'react';
import { GridColDef } from '@mui/x-data-grid-pro';
import { GridApiPremium } from '../../../models/gridApiPremium';
import { GridAggregationRule } from './gridAggregationInterfaces';
declare const AGGREGATION_WRAPPABLE_PROPERTIES: readonly ["valueGetter", "valueFormatter", "renderCell", "renderHeader", "filterOperators"];
declare type WrappableColumnProperty = typeof AGGREGATION_WRAPPABLE_PROPERTIES[number];
interface GridColDefWithAggregationWrappers extends GridColDef {
    aggregationWrappedProperties?: {
        [P in WrappableColumnProperty]?: {
            original: GridColDef[P];
            wrapped: GridColDef[P];
        };
    };
}
/**
 * Add a wrapper around each wrappable property of the column to customize the behavior of the aggregation cells.
 */
export declare const wrapColumnWithAggregationValue: ({ column, apiRef, aggregationRule, }: {
    column: GridColDef;
    apiRef: React.MutableRefObject<GridApiPremium>;
    aggregationRule: GridAggregationRule;
}) => GridColDef;
/**
 * Remove the aggregation wrappers around the wrappable properties of the column.
 */
export declare const unwrapColumnFromAggregation: ({ column, }: {
    column: GridColDefWithAggregationWrappers;
}) => GridColDef<any, any, any>;
export {};
