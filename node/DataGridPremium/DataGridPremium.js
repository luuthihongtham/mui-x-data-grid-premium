"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.DataGridPremium = void 0;

var React = _interopRequireWildcard(require("react"));

var _propTypes = _interopRequireDefault(require("prop-types"));

var _xLicensePro = require("@mui/x-license-pro");

var _utils = require("@mui/utils");

var _xDataGridPro = require("@mui/x-data-grid-pro");

var _internals = require("@mui/x-data-grid-pro/internals");

var _useDataGridPremiumComponent = require("./useDataGridPremiumComponent");

var _useDataGridPremiumProps = require("./useDataGridPremiumProps");

var _releaseInfo = require("../utils/releaseInfo");

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const releaseInfo = (0, _releaseInfo.getReleaseInfo)();
const DataGridPremiumRaw = /*#__PURE__*/React.forwardRef(function DataGridPremium(inProps, ref) {
  const props = (0, _useDataGridPremiumProps.useDataGridPremiumProps)(inProps);
  const apiRef = (0, _useDataGridPremiumComponent.useDataGridPremiumComponent)(props.apiRef, props);
  (0, _xLicensePro.useLicenseVerifier)('x-data-grid-premium', releaseInfo);
  return /*#__PURE__*/(0, _jsxRuntime.jsx)(_xDataGridPro.GridContextProvider, {
    apiRef: apiRef,
    props: props,
    children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_xDataGridPro.GridRoot, {
      className: props.className,
      style: props.style,
      sx: props.sx,
      ref: ref,
      children: /*#__PURE__*/(0, _jsxRuntime.jsxs)(_xDataGridPro.GridErrorHandler, {
        children: [/*#__PURE__*/(0, _jsxRuntime.jsx)(_xDataGridPro.GridHeaderPlaceholder, {}), /*#__PURE__*/(0, _jsxRuntime.jsx)(_xDataGridPro.GridBody, {
          ColumnHeadersComponent: _internals.DataGridProColumnHeaders,
          VirtualScrollerComponent: _internals.DataGridProVirtualScroller,
          children: /*#__PURE__*/(0, _jsxRuntime.jsx)(_xLicensePro.Watermark, {
            packageName: "x-data-grid-premium",
            releaseInfo: releaseInfo
          })
        }), /*#__PURE__*/(0, _jsxRuntime.jsx)(_xDataGridPro.GridFooterPlaceholder, {})]
      })
    })
  });
});
const DataGridPremium = /*#__PURE__*/React.memo(DataGridPremiumRaw);
exports.DataGridPremium = DataGridPremium;
process.env.NODE_ENV !== "production" ? DataGridPremiumRaw.propTypes = {
  // ----------------------------- Warning --------------------------------
  // | These PropTypes are generated from the TypeScript type definitions |
  // | To update them edit the TypeScript types and run "yarn proptypes"  |
  // ----------------------------------------------------------------------

  /**
   * Aggregation functions available on the grid.
   * @default GRID_AGGREGATION_FUNCTIONS
   */
  aggregationFunctions: _propTypes.default.object,

  /**
   * Set the aggregation model of the grid.
   */
  aggregationModel: _propTypes.default.object,

  /**
   * Rows used to generate the aggregated value.
   * If `filtered`, the aggregated values will be generated using only the rows currently passing the filtering process.
   * If `all`, the aggregated values will be generated using all the rows.
   * @default "filtered"
   */
  aggregationRowsScope: _propTypes.default.oneOf(['all', 'filtered']),

  /**
   * The ref object that allows grid manipulation. Can be instantiated with [[useGridApiRef()]].
   */
  apiRef: _propTypes.default.shape({
    current: _propTypes.default.object.isRequired
  }),

  /**
   * The label of the grid.
   */
  'aria-label': _propTypes.default.string,

  /**
   * The id of the element containing a label for the grid.
   */
  'aria-labelledby': _propTypes.default.string,

  /**
   * If `true`, the grid height is dynamic and follow the number of rows in the grid.
   * @default false
   */
  autoHeight: _propTypes.default.bool,

  /**
   * If `true`, the pageSize is calculated according to the container size and the max number of rows to avoid rendering a vertical scroll bar.
   * @default false
   */
  autoPageSize: _propTypes.default.bool,

  /**
   * Controls the modes of the cells.
   */
  cellModesModel: _propTypes.default.object,

  /**
   * If `true`, the grid get a first column with a checkbox that allows to select rows.
   * @default false
   */
  checkboxSelection: _propTypes.default.bool,

  /**
   * If `true`, the "Select All" header checkbox selects only the rows on the current page. To be used in combination with `checkboxSelection`.
   * It only works if the pagination is enabled.
   * @default false
   */
  checkboxSelectionVisibleOnly: (0, _utils.chainPropTypes)(_propTypes.default.bool, props => {
    if (!props.pagination && props.checkboxSelectionVisibleOnly) {
      return new Error('MUI: The `checkboxSelectionVisibleOnly` prop has no effect when the pagination is not enabled.');
    }

    return null;
  }),

  /**
   * Override or extend the styles applied to the component.
   */
  classes: _propTypes.default.object,

  /**
   * Number of extra columns to be rendered before/after the visible slice.
   * @default 3
   */
  columnBuffer: _propTypes.default.number,
  columnGroupingModel: _propTypes.default.arrayOf(_propTypes.default.object),

  /**
   * Set of columns of type [[GridColumns]].
   */
  columns: _propTypes.default.arrayOf(_propTypes.default.object).isRequired,

  /**
   * Number of rows from the `columnBuffer` that can be visible before a new slice is rendered.
   * @default 3
   */
  columnThreshold: _propTypes.default.number,

  /**
   * Extend native column types with your new column types.
   */
  columnTypes: _propTypes.default.object,

  /**
   * Set the column visibility model of the grid.
   * If defined, the grid will ignore the `hide` property in [[GridColDef]].
   */
  columnVisibilityModel: _propTypes.default.object,

  /**
   * Overrideable components.
   */
  components: _propTypes.default.object,

  /**
   * Overrideable components props dynamically passed to the component at rendering.
   */
  componentsProps: _propTypes.default.object,

  /**
   * If above 0, the row children will be expanded up to this depth.
   * If equal to -1, all the row children will be expanded.
   * @default 0
   */
  defaultGroupingExpansionDepth: _propTypes.default.number,

  /**
   * Set the density of the grid.
   * @default "standard"
   */
  density: _propTypes.default.oneOf(['comfortable', 'compact', 'standard']),

  /**
   * The row ids to show the detail panel.
   */
  detailPanelExpandedRowIds: _propTypes.default.arrayOf(_propTypes.default.oneOfType([_propTypes.default.number, _propTypes.default.string]).isRequired),

  /**
   * If `true`, aggregation is disabled.
   * @default false
   */
  disableAggregation: _propTypes.default.bool,

  /**
   * If `true`, the filtering will only be applied to the top level rows when grouping rows with the `treeData` prop.
   * @default false
   */
  disableChildrenFiltering: _propTypes.default.bool,

  /**
   * If `true`, the sorting will only be applied to the top level rows when grouping rows with the `treeData` prop.
   * @default false
   */
  disableChildrenSorting: _propTypes.default.bool,

  /**
   * If `true`, column filters are disabled.
   * @default false
   */
  disableColumnFilter: _propTypes.default.bool,

  /**
   * If `true`, the column menu is disabled.
   * @default false
   */
  disableColumnMenu: _propTypes.default.bool,

  /**
   * If `true`, the column pinning is disabled.
   * @default false
   */
  disableColumnPinning: _propTypes.default.bool,

  /**
   * If `true`, reordering columns is disabled.
   * @default false
   */
  disableColumnReorder: _propTypes.default.bool,

  /**
   * If `true`, resizing columns is disabled.
   * @default false
   */
  disableColumnResize: _propTypes.default.bool,

  /**
   * If `true`, hiding/showing columns is disabled.
   * @default false
   */
  disableColumnSelector: _propTypes.default.bool,

  /**
   * If `true`, the density selector is disabled.
   * @default false
   */
  disableDensitySelector: _propTypes.default.bool,

  /**
   * If `true`, rows will not be extended to fill the full width of the grid container.
   * @default false
   */
  disableExtendRowFullWidth: _propTypes.default.bool,

  /**
   * If `true`, modification to a cell will not be discarded if the mode is changed from "edit" to "view" while processing props.
   * @default false
   */
  disableIgnoreModificationsIfProcessingProps: _propTypes.default.bool,

  /**
   * If `true`, filtering with multiple columns is disabled.
   * @default false
   */
  disableMultipleColumnsFiltering: _propTypes.default.bool,

  /**
   * If `true`, sorting with multiple columns is disabled.
   * @default false
   */
  disableMultipleColumnsSorting: _propTypes.default.bool,

  /**
   * If `true`, multiple selection using the Ctrl or CMD key is disabled.
   * @default false
   */
  disableMultipleSelection: _propTypes.default.bool,

  /**
   * If `true`, the row grouping is disabled.
   * @default false
   */
  disableRowGrouping: _propTypes.default.bool,

  /**
   * If `true`, the selection on click on a row or cell is disabled.
   * @default false
   */
  disableSelectionOnClick: _propTypes.default.bool,

  /**
   * If `true`, the virtualization is disabled.
   * @default false
   */
  disableVirtualization: _propTypes.default.bool,

  /**
   * Controls whether to use the cell or row editing.
   * @default "cell"
   */
  editMode: _propTypes.default.oneOf(['cell', 'row']),

  /**
   * Set the edit rows model of the grid.
   */
  editRowsModel: _propTypes.default.object,

  /**
   * An error that will turn the grid into its error state and display the error component.
   */
  error: _propTypes.default.any,

  /**
   * Features under development.
   * For each feature, if the flag is not explicitly set to `true`, the feature will be fully disabled and any property / method call will not have any effect.
   */
  experimentalFeatures: _propTypes.default.shape({
    aggregation: _propTypes.default.bool,
    columnGrouping: _propTypes.default.bool,
    lazyLoading: _propTypes.default.bool,
    newEditingApi: _propTypes.default.bool,
    preventCommitWhileValidating: _propTypes.default.bool,
    rowPinning: _propTypes.default.bool,
    warnIfFocusStateIsNotSynced: _propTypes.default.bool
  }),

  /**
   * Filtering can be processed on the server or client-side.
   * Set it to 'server' if you would like to handle filtering on the server-side.
   * @default "client"
   */
  filterMode: (0, _utils.chainPropTypes)(_propTypes.default.oneOf(['client', 'server']), props => {
    if (props.treeData && props.filterMode === 'server') {
      return new Error('MUI: The `filterMode="server"` prop is not available when the `treeData` is enabled.');
    }

    return null;
  }),

  /**
   * Set the filter model of the grid.
   */
  filterModel: _propTypes.default.shape({
    items: _propTypes.default.arrayOf(_propTypes.default.shape({
      columnField: _propTypes.default.string.isRequired,
      id: _propTypes.default.oneOfType([_propTypes.default.number, _propTypes.default.string]),
      operatorValue: _propTypes.default.string,
      value: _propTypes.default.any
    })).isRequired,
    linkOperator: _propTypes.default.oneOf(['and', 'or']),
    quickFilterLogicOperator: _propTypes.default.oneOf(['and', 'or']),
    quickFilterValues: _propTypes.default.array
  }),

  /**
   * Determines the position of an aggregated value.
   * @param {GridRowTreeNodeConfig | null} groupNode The current group (`null` being the top level group).
   * @returns {GridAggregationPosition | null} Position of the aggregated value (if `null`, the group will not be aggregated).
   * @default `(groupNode) => groupNode == null ? 'footer' : 'inline'`
   */
  getAggregationPosition: _propTypes.default.func,

  /**
   * Function that applies CSS classes dynamically on cells.
   * @param {GridCellParams} params With all properties from [[GridCellParams]].
   * @returns {string} The CSS class to apply to the cell.
   */
  getCellClassName: _propTypes.default.func,

  /**
   * Function that returns the element to render in row detail.
   * @param {GridRowParams} params With all properties from [[GridRowParams]].
   * @returns {JSX.Element} The row detail element.
   */
  getDetailPanelContent: _propTypes.default.func,

  /**
   * Function that returns the height of the row detail panel.
   * @param {GridRowParams} params With all properties from [[GridRowParams]].
   * @returns {number | string} The height in pixels or "auto" to use the content height.
   * @default "() => 500"
   */
  getDetailPanelHeight: _propTypes.default.func,

  /**
   * Function that returns the estimated height for a row.
   * Only works if dynamic row height is used.
   * Once the row height is measured this value is discarded.
   * @param {GridRowHeightParams} params With all properties from [[GridRowHeightParams]].
   * @returns {number | null} The estimated row height value. If `null` or `undefined` then the default row height, based on the density, is applied.
   */
  getEstimatedRowHeight: _propTypes.default.func,

  /**
   * Function that applies CSS classes dynamically on rows.
   * @param {GridRowClassNameParams} params With all properties from [[GridRowClassNameParams]].
   * @returns {string} The CSS class to apply to the row.
   */
  getRowClassName: _propTypes.default.func,

  /**
   * Function that sets the row height per row.
   * @param {GridRowHeightParams} params With all properties from [[GridRowHeightParams]].
   * @returns {GridRowHeightReturnValue} The row height value. If `null` or `undefined` then the default row height is applied. If "auto" then the row height is calculated based on the content.
   */
  getRowHeight: _propTypes.default.func,

  /**
   * Return the id of a given [[GridRowModel]].
   */
  getRowId: _propTypes.default.func,

  /**
   * Function that allows to specify the spacing between rows.
   * @param {GridRowSpacingParams} params With all properties from [[GridRowSpacingParams]].
   * @returns {GridRowSpacing} The row spacing values.
   */
  getRowSpacing: _propTypes.default.func,

  /**
   * Determines the path of a row in the tree data.
   * For instance, a row with the path ["A", "B"] is the child of the row with the path ["A"].
   * Note that all paths must contain at least one element.
   * @template R
   * @param {R} row The row from which we want the path.
   * @returns {string[]} The path to the row.
   */
  getTreeDataPath: _propTypes.default.func,

  /**
   * The grouping column used by the tree data.
   */
  groupingColDef: _propTypes.default.oneOfType([_propTypes.default.func, _propTypes.default.object]),

  /**
   * Set the height in pixel of the column headers in the grid.
   * @default 56
   */
  headerHeight: _propTypes.default.number,

  /**
   * If `true`, the footer component is hidden.
   * @default false
   */
  hideFooter: _propTypes.default.bool,

  /**
   * If `true`, the pagination component in the footer is hidden.
   * @default false
   */
  hideFooterPagination: _propTypes.default.bool,

  /**
   * If `true`, the row count in the footer is hidden.
   * It has no effect if the pagination is enabled.
   * @default false
   */
  hideFooterRowCount: (0, _utils.chainPropTypes)(_propTypes.default.bool, props => {
    if (props.pagination && props.hideFooterRowCount) {
      return new Error('MUI: The `hideFooterRowCount` prop has no effect when the pagination is enabled.');
    }

    return null;
  }),

  /**
   * If `true`, the selected row count in the footer is hidden.
   * @default false
   */
  hideFooterSelectedRowCount: _propTypes.default.bool,

  /**
   * The initial state of the DataGridPremium.
   * The data in it will be set in the state on initialization but will not be controlled.
   * If one of the data in `initialState` is also being controlled, then the control state wins.
   */
  initialState: _propTypes.default.object,

  /**
   * Callback fired when a cell is rendered, returns true if the cell is editable.
   * @param {GridCellParams} params With all properties from [[GridCellParams]].
   * @returns {boolean} A boolean indicating if the cell is editable.
   */
  isCellEditable: _propTypes.default.func,

  /**
   * Determines if a group should be expanded after its creation.
   * This prop takes priority over the `defaultGroupingExpansionDepth` prop.
   * @param {GridRowTreeNodeConfig} node The node of the group to test.
   * @returns {boolean} A boolean indicating if the group is expanded.
   */
  isGroupExpandedByDefault: _propTypes.default.func,

  /**
   * Determines if a row can be selected.
   * @param {GridRowParams} params With all properties from [[GridRowParams]].
   * @returns {boolean} A boolean indicating if the cell is selectable.
   */
  isRowSelectable: _propTypes.default.func,

  /**
   * If `true`, the selection model will retain selected rows that do not exist.
   * Useful when using server side pagination and row selections need to be retained
   * when changing pages.
   * @default false
   */
  keepNonExistentRowsSelected: _propTypes.default.bool,

  /**
   * If `true`, a  loading overlay is displayed.
   */
  loading: _propTypes.default.bool,

  /**
   * Set the locale text of the grid.
   * You can find all the translation keys supported in [the source](https://github.com/mui/mui-x/blob/HEAD/packages/grid/x-data-grid/src/constants/localeTextConstants.ts) in the GitHub repository.
   */
  localeText: _propTypes.default.object,

  /**
   * Pass a custom logger in the components that implements the [[Logger]] interface.
   * @default console
   */
  logger: _propTypes.default.shape({
    debug: _propTypes.default.func.isRequired,
    error: _propTypes.default.func.isRequired,
    info: _propTypes.default.func.isRequired,
    warn: _propTypes.default.func.isRequired
  }),

  /**
   * Allows to pass the logging level or false to turn off logging.
   * @default "error" ("warn" in dev mode)
   */
  logLevel: _propTypes.default.oneOf(['debug', 'error', 'info', 'warn', false]),

  /**
   * Nonce of the inline styles for [Content Security Policy](https://www.w3.org/TR/2016/REC-CSP2-20161215/#script-src-the-nonce-attribute).
   */
  nonce: _propTypes.default.string,

  /**
   * Callback fired when the row grouping model changes.
   * @param {GridAggregationModel} model The aggregated columns.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onAggregationModelChange: _propTypes.default.func,

  /**
   * Callback fired when any cell is clicked.
   * @param {GridCellParams} params With all properties from [[GridCellParams]].
   * @param {MuiEvent<React.MouseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onCellClick: _propTypes.default.func,

  /**
   * Callback fired when a double click event comes from a cell element.
   * @param {GridCellParams} params With all properties from [[GridCellParams]].
   * @param {MuiEvent<React.MouseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onCellDoubleClick: _propTypes.default.func,

  /**
   * Callback fired when the cell changes are committed.
   * @param {GridCellEditCommitParams} params With all properties from [[GridCellEditCommitParams]].
   * @param {MuiEvent<MuiBaseEvent>} event The event that caused this prop to be called.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onCellEditCommit: _propTypes.default.func,

  /**
   * Callback fired when the cell turns to edit mode.
   * @param {GridCellParams} params With all properties from [[GridCellParams]].
   * @param {MuiEvent<React.KeyboardEvent | React.MouseEvent>} event The event that caused this prop to be called.
   */
  onCellEditStart: _propTypes.default.func,

  /**
   * Callback fired when the cell turns to view mode.
   * @param {GridCellParams} params With all properties from [[GridCellParams]].
   * @param {MuiEvent<MuiBaseEvent>} event The event that caused this prop to be called.
   */
  onCellEditStop: _propTypes.default.func,

  /**
   * Callback fired when a cell loses focus.
   * @param {GridCellParams} params With all properties from [[GridCellParams]].
   * @param {MuiEvent<MuiBaseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onCellFocusOut: _propTypes.default.func,

  /**
   * Callback fired when a keydown event comes from a cell element.
   * @param {GridCellParams} params With all properties from [[GridCellParams]].
   * @param {MuiEvent<React.KeyboardEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onCellKeyDown: _propTypes.default.func,

  /**
   * Callback fired when the `cellModesModel` prop changes.
   * @param {GridCellModesModel} cellModesModel Object containig which cells are in "edit" mode.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onCellModesModelChange: _propTypes.default.func,

  /**
   * Callback fired when a click event comes from a column header element.
   * @param {GridColumnHeaderParams} params With all properties from [[GridColumnHeaderParams]].
   * @param {MuiEvent<React.MouseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onColumnHeaderClick: _propTypes.default.func,

  /**
   * Callback fired when a double click event comes from a column header element.
   * @param {GridColumnHeaderParams} params With all properties from [[GridColumnHeaderParams]].
   * @param {MuiEvent<React.MouseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onColumnHeaderDoubleClick: _propTypes.default.func,

  /**
   * Callback fired when a mouse enter event comes from a column header element.
   * @param {GridColumnHeaderParams} params With all properties from [[GridColumnHeaderParams]].
   * @param {MuiEvent<React.MouseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onColumnHeaderEnter: _propTypes.default.func,

  /**
   * Callback fired when a mouse leave event comes from a column header element.
   * @param {GridColumnHeaderParams} params With all properties from [[GridColumnHeaderParams]].
   * @param {MuiEvent<React.MouseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onColumnHeaderLeave: _propTypes.default.func,

  /**
   * Callback fired when a mouseout event comes from a column header element.
   * @param {GridColumnHeaderParams} params With all properties from [[GridColumnHeaderParams]].
   * @param {MuiEvent<React.MouseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onColumnHeaderOut: _propTypes.default.func,

  /**
   * Callback fired when a mouseover event comes from a column header element.
   * @param {GridColumnHeaderParams} params With all properties from [[GridColumnHeaderParams]].
   * @param {MuiEvent<React.MouseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onColumnHeaderOver: _propTypes.default.func,

  /**
   * Callback fired when a column is reordered.
   * @param {GridColumnOrderChangeParams} params With all properties from [[GridColumnOrderChangeParams]].
   * @param {MuiEvent<{}>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onColumnOrderChange: _propTypes.default.func,

  /**
   * Callback fired while a column is being resized.
   * @param {GridColumnResizeParams} params With all properties from [[GridColumnResizeParams]].
   * @param {MuiEvent<React.MouseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onColumnResize: _propTypes.default.func,

  /**
   * Callback fired when a column visibility changes.
   * Only works when no `columnVisibilityModel` is provided and if we change the visibility of a single column at a time.
   * @param {GridColumnVisibilityChangeParams} params With all properties from [[GridColumnVisibilityChangeParams]].
   * @param {MuiEvent<{}>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   * @deprecated Use `onColumnVisibilityModelChange` instead.
   */
  onColumnVisibilityChange: _propTypes.default.func,

  /**
   * Callback fired when the column visibility model changes.
   * @param {GridColumnVisibilityModel} model The new model.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onColumnVisibilityModelChange: _propTypes.default.func,

  /**
   * Callback fired when the width of a column is changed.
   * @param {GridColumnResizeParams} params With all properties from [[GridColumnResizeParams]].
   * @param {MuiEvent<React.MouseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onColumnWidthChange: _propTypes.default.func,

  /**
   * Callback fired when the detail panel of a row is opened or closed.
   * @param {GridRowId[]} ids The ids of the rows which have the detail panel open.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onDetailPanelExpandedRowIdsChange: _propTypes.default.func,

  /**
   * Callback fired when the edit cell value changes.
   * @param {GridEditCellPropsParams} params With all properties from [[GridEditCellPropsParams]].
   * @param {MuiEvent<React.SyntheticEvent>} event The event that caused this prop to be called.
   * @param {GridCallbackDetails} details Additional details for this callback.
   * @deprecated use `preProcessEditCellProps` from the [`GridColDef`](/x/api/data-grid/grid-col-def/)
   */
  onEditCellPropsChange: _propTypes.default.func,

  /**
   * Callback fired when the `editRowsModel` changes.
   * @param {GridEditRowsModel} editRowsModel With all properties from [[GridEditRowsModel]].
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onEditRowsModelChange: _propTypes.default.func,

  /**
   * Callback fired when an exception is thrown in the grid.
   * @param {any} args The arguments passed to the `showError` call.
   * @param {MuiEvent<{}>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onError: _propTypes.default.func,

  /**
   * Callback fired when rowCount is set and the next batch of virtualized rows is rendered.
   * @param {GridFetchRowsParams} params With all properties from [[GridFetchRowsParams]].
   * @param {MuiEvent<{}>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onFetchRows: _propTypes.default.func,

  /**
   * Callback fired when the Filter model changes before the filters are applied.
   * @param {GridFilterModel} model With all properties from [[GridFilterModel]].
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onFilterModelChange: _propTypes.default.func,

  /**
   * Callback fired when the menu is closed.
   * @param {GridMenuParams} params With all properties from [[GridMenuParams]].
   * @param {MuiEvent<{}>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onMenuClose: _propTypes.default.func,

  /**
   * Callback fired when the menu is opened.
   * @param {GridMenuParams} params With all properties from [[GridMenuParams]].
   * @param {MuiEvent<{}>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onMenuOpen: _propTypes.default.func,

  /**
   * Callback fired when the current page has changed.
   * @param {number} page Index of the page displayed on the Grid.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onPageChange: _propTypes.default.func,

  /**
   * Callback fired when the page size has changed.
   * @param {number} pageSize Size of the page displayed on the Grid.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onPageSizeChange: _propTypes.default.func,

  /**
   * Callback fired when the pinned columns have changed.
   * @param {GridPinnedColumns} pinnedColumns The changed pinned columns.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onPinnedColumnsChange: _propTypes.default.func,

  /**
   * Callback fired when the preferences panel is closed.
   * @param {GridPreferencePanelParams} params With all properties from [[GridPreferencePanelParams]].
   * @param {MuiEvent<{}>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onPreferencePanelClose: _propTypes.default.func,

  /**
   * Callback fired when the preferences panel is opened.
   * @param {GridPreferencePanelParams} params With all properties from [[GridPreferencePanelParams]].
   * @param {MuiEvent<{}>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onPreferencePanelOpen: _propTypes.default.func,

  /**
   * Callback called when `processRowUpdate` throws an error or rejects.
   * @param {any} error The error thrown.
   */
  onProcessRowUpdateError: _propTypes.default.func,

  /**
   * Callback fired when the grid is resized.
   * @param {ElementSize} containerSize With all properties from [[ElementSize]].
   * @param {MuiEvent<{}>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onResize: _propTypes.default.func,

  /**
   * Callback fired when a row is clicked.
   * Not called if the target clicked is an interactive element added by the built-in columns.
   * @param {GridRowParams} params With all properties from [[GridRowParams]].
   * @param {MuiEvent<React.MouseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onRowClick: _propTypes.default.func,

  /**
   * Callback fired when a double click event comes from a row container element.
   * @param {GridRowParams} params With all properties from [[RowParams]].
   * @param {MuiEvent<React.MouseEvent>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onRowDoubleClick: _propTypes.default.func,

  /**
   * Callback fired when the row changes are committed.
   * @param {GridRowId} id The row id.
   * @param {MuiEvent<MuiBaseEvent>} event The event that caused this prop to be called.
   */
  onRowEditCommit: _propTypes.default.func,

  /**
   * Callback fired when the row turns to edit mode.
   * @param {GridRowParams} params With all properties from [[GridRowParams]].
   * @param {MuiEvent<React.KeyboardEvent | React.MouseEvent>} event The event that caused this prop to be called.
   */
  onRowEditStart: _propTypes.default.func,

  /**
   * Callback fired when the row turns to view mode.
   * @param {GridRowParams} params With all properties from [[GridRowParams]].
   * @param {MuiEvent<MuiBaseEvent>} event The event that caused this prop to be called.
   */
  onRowEditStop: _propTypes.default.func,

  /**
   * Callback fired when the row grouping model changes.
   * @param {GridRowGroupingModel} model Columns used as grouping criteria.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onRowGroupingModelChange: _propTypes.default.func,

  /**
   * Callback fired when the `rowModesModel` prop changes.
   * @param {GridRowModesModel} rowModesModel Object containig which rows are in "edit" mode.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onRowModesModelChange: _propTypes.default.func,

  /**
   * Callback fired when a row is being reordered.
   * @param {GridRowOrderChangeParams} params With all properties from [[GridRowOrderChangeParams]].
   * @param {MuiEvent<{}>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onRowOrderChange: _propTypes.default.func,

  /**
   * Callback fired when scrolling to the bottom of the grid viewport.
   * @param {GridRowScrollEndParams} params With all properties from [[GridRowScrollEndParams]].
   * @param {MuiEvent<{}>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onRowsScrollEnd: _propTypes.default.func,

  /**
   * Callback fired when the selection state of one or multiple rows changes.
   * @param {GridSelectionModel} selectionModel With all the row ids [[GridSelectionModel]].
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onSelectionModelChange: _propTypes.default.func,

  /**
   * Callback fired when the sort model changes before a column is sorted.
   * @param {GridSortModel} model With all properties from [[GridSortModel]].
   * @param {GridCallbackDetails} details Additional details for this callback.
   */
  onSortModelChange: _propTypes.default.func,

  /**
   * Callback fired when the state of the grid is updated.
   * @param {GridState} state The new state.
   * @param {MuiEvent<{}>} event The event object.
   * @param {GridCallbackDetails} details Additional details for this callback.
   * @ignore - do not document.
   */
  onStateChange: _propTypes.default.func,

  /**
   * The zero-based index of the current page.
   * @default 0
   */
  page: _propTypes.default.number,

  /**
   * Set the number of rows in one page.
   * If some of the rows have children (for instance in the tree data), this number represents the amount of top level rows wanted on each page.
   * @default 100
   */
  pageSize: _propTypes.default.number,

  /**
   * If `true`, pagination is enabled.
   * @default false
   */
  pagination: _propTypes.default.bool,

  /**
   * Pagination can be processed on the server or client-side.
   * Set it to 'client' if you would like to handle the pagination on the client-side.
   * Set it to 'server' if you would like to handle the pagination on the server-side.
   * @default "client"
   */
  paginationMode: _propTypes.default.oneOf(['client', 'server']),

  /**
   * The column fields to display pinned to left or right.
   */
  pinnedColumns: _propTypes.default.shape({
    left: _propTypes.default.arrayOf(_propTypes.default.string),
    right: _propTypes.default.arrayOf(_propTypes.default.string)
  }),

  /**
   * Rows data to pin on top or bottom.
   */
  pinnedRows: _propTypes.default.shape({
    bottom: _propTypes.default.array,
    top: _propTypes.default.array
  }),

  /**
   * Callback called before updating a row with new values in the row and cell editing.
   * Only applied if `props.experimentalFeatures.newEditingApi: true`.
   * @template R
   * @param {R} newRow Row object with the new values.
   * @param {R} oldRow Row object with the old values.
   * @returns {Promise<R> | R} The final values to update the row.
   */
  processRowUpdate: _propTypes.default.func,

  /**
   * Number of extra rows to be rendered before/after the visible slice.
   * @default 3
   */
  rowBuffer: _propTypes.default.number,

  /**
   * Set the total number of rows, if it is different from the length of the value `rows` prop.
   * If some rows have children (for instance in the tree data), this number represents the amount of top level rows.
   */
  rowCount: _propTypes.default.number,

  /**
   * If `single`, all column we are grouping by will be represented in the same grouping the same column.
   * If `multiple`, each column we are grouping by will be represented in its own column.
   * @default 'single'
   */
  rowGroupingColumnMode: _propTypes.default.oneOf(['multiple', 'single']),

  /**
   * Set the row grouping model of the grid.
   */
  rowGroupingModel: _propTypes.default.arrayOf(_propTypes.default.string),

  /**
   * Set the height in pixel of a row in the grid.
   * @default 52
   */
  rowHeight: _propTypes.default.number,

  /**
   * Controls the modes of the rows.
   */
  rowModesModel: _propTypes.default.object,

  /**
   * If `true`, the reordering of rows is enabled.
   * @default false
   */
  rowReordering: _propTypes.default.bool,

  /**
   * Set of rows of type [[GridRowsProp]].
   */
  rows: _propTypes.default.array.isRequired,

  /**
   * Loading rows can be processed on the server or client-side.
   * Set it to 'client' if you would like enable infnite loading.
   * Set it to 'server' if you would like to enable lazy loading.
   * * @default "client"
   */
  rowsLoadingMode: _propTypes.default.oneOf(['client', 'server']),

  /**
   * Sets the type of space between rows added by `getRowSpacing`.
   * @default "margin"
   */
  rowSpacingType: _propTypes.default.oneOf(['border', 'margin']),

  /**
   * Select the pageSize dynamically using the component UI.
   * @default [25, 50, 100]
   */
  rowsPerPageOptions: _propTypes.default.arrayOf(_propTypes.default.number),

  /**
   * Number of rows from the `rowBuffer` that can be visible before a new slice is rendered.
   * @default 3
   */
  rowThreshold: _propTypes.default.number,

  /**
   * Override the height/width of the grid inner scrollbar.
   */
  scrollbarSize: _propTypes.default.number,

  /**
   * Set the area in `px` at the bottom of the grid viewport where onRowsScrollEnd is called.
   * @default 80
   */
  scrollEndThreshold: _propTypes.default.number,

  /**
   * Set the selection model of the grid.
   */
  selectionModel: _propTypes.default.oneOfType([_propTypes.default.arrayOf(_propTypes.default.oneOfType([_propTypes.default.number, _propTypes.default.string]).isRequired), _propTypes.default.number, _propTypes.default.string]),

  /**
   * If `true`, the right border of the cells are displayed.
   * @default false
   */
  showCellRightBorder: _propTypes.default.bool,

  /**
   * If `true`, the right border of the column headers are displayed.
   * @default false
   */
  showColumnRightBorder: _propTypes.default.bool,

  /**
   * Sorting can be processed on the server or client-side.
   * Set it to 'client' if you would like to handle sorting on the client-side.
   * Set it to 'server' if you would like to handle sorting on the server-side.
   * @default "client"
   */
  sortingMode: _propTypes.default.oneOf(['client', 'server']),

  /**
   * The order of the sorting sequence.
   * @default ['asc', 'desc', null]
   */
  sortingOrder: _propTypes.default.arrayOf(_propTypes.default.oneOf(['asc', 'desc'])),

  /**
   * Set the sort model of the grid.
   */
  sortModel: _propTypes.default.arrayOf(_propTypes.default.shape({
    field: _propTypes.default.string.isRequired,
    sort: _propTypes.default.oneOf(['asc', 'desc'])
  })),

  /**
   * The system prop that allows defining system overrides as well as additional CSS styles.
   */
  sx: _propTypes.default.oneOfType([_propTypes.default.arrayOf(_propTypes.default.oneOfType([_propTypes.default.func, _propTypes.default.object, _propTypes.default.bool])), _propTypes.default.func, _propTypes.default.object]),

  /**
   * If positive, the Grid will throttle updates coming from `apiRef.current.updateRows` and `apiRef.current.setRows`.
   * It can be useful if you have a high update rate but do not want to do heavy work like filtering / sorting or rendering on each  individual update.
   * @default 0
   */
  throttleRowsMs: _propTypes.default.number,

  /**
   * If `true`, the rows will be gathered in a tree structure according to the `getTreeDataPath` prop.
   * @default false
   */
  treeData: _propTypes.default.bool
} : void 0;