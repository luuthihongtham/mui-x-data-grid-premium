"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useDataGridPremiumProps = exports.DATA_GRID_PREMIUM_PROPS_DEFAULT_VALUES = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var React = _interopRequireWildcard(require("react"));

var _styles = require("@mui/material/styles");

var _xDataGridPro = require("@mui/x-data-grid-pro");

var _aggregation = require("../hooks/features/aggregation");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

/**
 * The default values of `DataGridPremiumPropsWithDefaultValue` to inject in the props of DataGridPremium.
 */
const DATA_GRID_PREMIUM_PROPS_DEFAULT_VALUES = (0, _extends2.default)({}, _xDataGridPro.DATA_GRID_PRO_PROPS_DEFAULT_VALUES, {
  disableAggregation: false,
  disableRowGrouping: false,
  rowGroupingColumnMode: 'single',
  aggregationFunctions: _aggregation.GRID_AGGREGATION_FUNCTIONS,
  aggregationRowsScope: 'filtered',
  getAggregationPosition: groupNode => groupNode == null ? 'footer' : 'inline'
});
exports.DATA_GRID_PREMIUM_PROPS_DEFAULT_VALUES = DATA_GRID_PREMIUM_PROPS_DEFAULT_VALUES;

const useDataGridPremiumProps = inProps => {
  const themedProps = (0, _styles.useThemeProps)({
    props: inProps,
    name: 'MuiDataGrid'
  });
  const localeText = React.useMemo(() => (0, _extends2.default)({}, _xDataGridPro.GRID_DEFAULT_LOCALE_TEXT, themedProps.localeText), [themedProps.localeText]);
  const components = React.useMemo(() => {
    const overrides = themedProps.components;

    if (!overrides) {
      return (0, _extends2.default)({}, _xDataGridPro.DATA_GRID_DEFAULT_SLOTS_COMPONENTS);
    }

    const mergedComponents = {};
    Object.entries(_xDataGridPro.DATA_GRID_DEFAULT_SLOTS_COMPONENTS).forEach(([key, defaultComponent]) => {
      mergedComponents[key] = overrides[key] === undefined ? defaultComponent : overrides[key];
    });
    return mergedComponents;
  }, [themedProps.components]);
  return React.useMemo(() => {
    var _themedProps$experime;

    return (0, _extends2.default)({}, DATA_GRID_PREMIUM_PROPS_DEFAULT_VALUES, themedProps, {
      disableAggregation: themedProps.disableAggregation || !((_themedProps$experime = themedProps.experimentalFeatures) != null && _themedProps$experime.aggregation),
      localeText,
      components,
      signature: 'DataGridPremium'
    });
  }, [themedProps, localeText, components]);
};

exports.useDataGridPremiumProps = useDataGridPremiumProps;