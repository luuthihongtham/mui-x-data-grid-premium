"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGridApiRef = void 0;

var _xDataGridPro = require("@mui/x-data-grid-pro");

const useGridApiRef = _xDataGridPro.useGridApiRef;
exports.useGridApiRef = useGridApiRef;