"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGridApiContext = void 0;

var _xDataGridPro = require("@mui/x-data-grid-pro");

const useGridApiContext = _xDataGridPro.useGridApiContext;
exports.useGridApiContext = useGridApiContext;