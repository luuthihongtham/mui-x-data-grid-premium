"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.gridAggregationStateSelector = exports.gridAggregationModelSelector = exports.gridAggregationLookupSelector = void 0;

var _internals = require("@mui/x-data-grid-pro/internals");

const gridAggregationStateSelector = state => state.aggregation;

exports.gridAggregationStateSelector = gridAggregationStateSelector;
const gridAggregationModelSelector = (0, _internals.createSelector)(gridAggregationStateSelector, aggregationState => aggregationState.model);
exports.gridAggregationModelSelector = gridAggregationModelSelector;
const gridAggregationLookupSelector = (0, _internals.createSelector)(gridAggregationStateSelector, aggregationState => aggregationState.lookup);
exports.gridAggregationLookupSelector = gridAggregationLookupSelector;