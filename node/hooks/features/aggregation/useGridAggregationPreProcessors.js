"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGridAggregationPreProcessors = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var React = _interopRequireWildcard(require("react"));

var _Divider = _interopRequireDefault(require("@mui/material/Divider"));

var _xDataGridPro = require("@mui/x-data-grid-pro");

var _internals = require("@mui/x-data-grid-pro/internals");

var _gridAggregationUtils = require("./gridAggregationUtils");

var _wrapColumnWithAggregation = require("./wrapColumnWithAggregation");

var _GridAggregationColumnMenuItem = require("../../../components/GridAggregationColumnMenuItem");

var _gridAggregationSelectors = require("./gridAggregationSelectors");

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const Divider = () => /*#__PURE__*/(0, _jsxRuntime.jsx)(_Divider.default, {
  onClick: event => event.stopPropagation()
});

const useGridAggregationPreProcessors = (apiRef, props) => {
  const updateAggregatedColumns = React.useCallback(columnsState => {
    const {
      rulesOnLastColumnHydration
    } = apiRef.current.unstable_caches.aggregation;
    const aggregationRules = props.disableAggregation ? {} : (0, _gridAggregationUtils.getAggregationRules)({
      columnsLookup: columnsState.lookup,
      aggregationModel: (0, _gridAggregationSelectors.gridAggregationModelSelector)(apiRef),
      aggregationFunctions: props.aggregationFunctions
    });
    columnsState.all.forEach(field => {
      const shouldHaveAggregationValue = !!aggregationRules[field];
      const haveAggregationColumnValue = !!rulesOnLastColumnHydration[field];
      let column = columnsState.lookup[field];

      if (haveAggregationColumnValue) {
        column = (0, _wrapColumnWithAggregation.unwrapColumnFromAggregation)({
          column
        });
      }

      if (shouldHaveAggregationValue) {
        column = (0, _wrapColumnWithAggregation.wrapColumnWithAggregationValue)({
          column,
          aggregationRule: aggregationRules[field],
          apiRef
        });
      }

      columnsState.lookup[field] = column;
    });
    apiRef.current.unstable_caches.aggregation.rulesOnLastColumnHydration = aggregationRules;
    return columnsState;
  }, [apiRef, props.aggregationFunctions, props.disableAggregation]);
  const addGroupFooterRows = React.useCallback(groupingParams => {
    let newGroupingParams;
    let rulesOnLastRowHydration;

    if (props.disableAggregation) {
      newGroupingParams = groupingParams;
      rulesOnLastRowHydration = {};
    } else {
      const aggregationRules = (0, _gridAggregationUtils.getAggregationRules)({
        columnsLookup: (0, _xDataGridPro.gridColumnLookupSelector)(apiRef),
        aggregationModel: (0, _gridAggregationSelectors.gridAggregationModelSelector)(apiRef),
        aggregationFunctions: props.aggregationFunctions
      });
      rulesOnLastRowHydration = aggregationRules; // If no column have an aggregation rule
      // Then don't create the footer rows

      if (Object.values(aggregationRules).length === 0) {
        newGroupingParams = groupingParams;
      } else {
        newGroupingParams = (0, _gridAggregationUtils.addFooterRows)({
          groupingParams,
          aggregationRules,
          getAggregationPosition: props.getAggregationPosition,
          apiRef
        });
      }
    }

    apiRef.current.unstable_caches.aggregation.rulesOnLastRowHydration = rulesOnLastRowHydration;
    return newGroupingParams;
  }, [apiRef, props.disableAggregation, props.getAggregationPosition, props.aggregationFunctions]);
  const addColumnMenuButtons = React.useCallback((initialValue, column) => {
    if (props.disableAggregation) {
      return initialValue;
    }

    const availableAggregationFunctions = (0, _gridAggregationUtils.getAvailableAggregationFunctions)({
      aggregationFunctions: props.aggregationFunctions,
      column
    });

    if (availableAggregationFunctions.length === 0) {
      return initialValue;
    }

    return [...initialValue, /*#__PURE__*/(0, _jsxRuntime.jsx)(Divider, {}), /*#__PURE__*/(0, _jsxRuntime.jsx)(_GridAggregationColumnMenuItem.GridAggregationColumnMenuItem, {
      column: column,
      label: apiRef.current.getLocaleText('aggregationMenuItemHeader'),
      availableAggregationFunctions: availableAggregationFunctions
    })];
  }, [apiRef, props.aggregationFunctions, props.disableAggregation]);
  const stateExportPreProcessing = React.useCallback(prevState => {
    if (props.disableAggregation) {
      return prevState;
    }

    const aggregationModelToExport = (0, _gridAggregationSelectors.gridAggregationModelSelector)(apiRef);

    if (Object.values(aggregationModelToExport).length === 0) {
      return prevState;
    }

    return (0, _extends2.default)({}, prevState, {
      aggregation: {
        model: aggregationModelToExport
      }
    });
  }, [apiRef, props.disableAggregation]);
  const stateRestorePreProcessing = React.useCallback((params, context) => {
    var _context$stateToResto;

    if (props.disableAggregation) {
      return params;
    }

    const aggregationModel = (_context$stateToResto = context.stateToRestore.aggregation) == null ? void 0 : _context$stateToResto.model;

    if (aggregationModel != null) {
      apiRef.current.setState((0, _gridAggregationUtils.mergeStateWithAggregationModel)(aggregationModel));
    }

    return params;
  }, [apiRef, props.disableAggregation]);
  (0, _internals.useGridRegisterPipeProcessor)(apiRef, 'hydrateColumns', updateAggregatedColumns);
  (0, _internals.useGridRegisterPipeProcessor)(apiRef, 'hydrateRows', addGroupFooterRows);
  (0, _internals.useGridRegisterPipeProcessor)(apiRef, 'columnMenu', addColumnMenuButtons);
  (0, _internals.useGridRegisterPipeProcessor)(apiRef, 'exportState', stateExportPreProcessing);
  (0, _internals.useGridRegisterPipeProcessor)(apiRef, 'restoreState', stateRestorePreProcessing);
};

exports.useGridAggregationPreProcessors = useGridAggregationPreProcessors;