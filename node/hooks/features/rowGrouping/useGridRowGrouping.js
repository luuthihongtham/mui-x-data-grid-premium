"use strict";

var _interopRequireDefault = require("@babel/runtime/helpers/interopRequireDefault");

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGridRowGrouping = exports.rowGroupingStateInitializer = void 0;

var _extends2 = _interopRequireDefault(require("@babel/runtime/helpers/extends"));

var React = _interopRequireWildcard(require("react"));

var _Divider = _interopRequireDefault(require("@mui/material/Divider"));

var _xDataGridPro = require("@mui/x-data-grid-pro");

var _internals = require("@mui/x-data-grid-pro/internals");

var _gridRowGroupingSelector = require("./gridRowGroupingSelector");

var _gridRowGroupingUtils = require("./gridRowGroupingUtils");

var _GridRowGroupableColumnMenuItems = require("../../../components/GridRowGroupableColumnMenuItems");

var _GridRowGroupingColumnMenuItems = require("../../../components/GridRowGroupingColumnMenuItems");

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

const Divider = () => /*#__PURE__*/(0, _jsxRuntime.jsx)(_Divider.default, {
  onClick: event => event.stopPropagation()
});

const rowGroupingStateInitializer = (state, props, apiRef) => {
  var _ref, _props$rowGroupingMod, _props$initialState, _props$initialState$r;

  apiRef.current.unstable_caches.rowGrouping = {
    rulesOnLastRowTreeCreation: []
  };
  return (0, _extends2.default)({}, state, {
    rowGrouping: {
      model: (_ref = (_props$rowGroupingMod = props.rowGroupingModel) != null ? _props$rowGroupingMod : (_props$initialState = props.initialState) == null ? void 0 : (_props$initialState$r = _props$initialState.rowGrouping) == null ? void 0 : _props$initialState$r.model) != null ? _ref : []
    }
  });
};
/**
 * @requires useGridColumns (state, method) - can be after, async only
 * @requires useGridRows (state, method) - can be after, async only
 * @requires useGridParamsApi (method) - can be after, async only
 */


exports.rowGroupingStateInitializer = rowGroupingStateInitializer;

const useGridRowGrouping = (apiRef, props) => {
  var _props$initialState3, _props$initialState3$;

  apiRef.current.unstable_registerControlState({
    stateId: 'rowGrouping',
    propModel: props.rowGroupingModel,
    propOnChange: props.onRowGroupingModelChange,
    stateSelector: _gridRowGroupingSelector.gridRowGroupingModelSelector,
    changeEvent: 'rowGroupingModelChange'
  });
  /**
   * API METHODS
   */

  const setRowGroupingModel = React.useCallback(model => {
    const currentModel = (0, _gridRowGroupingSelector.gridRowGroupingModelSelector)(apiRef);

    if (currentModel !== model) {
      apiRef.current.setState((0, _gridRowGroupingUtils.mergeStateWithRowGroupingModel)(model));
      (0, _gridRowGroupingUtils.setStrategyAvailability)(apiRef, props.disableRowGrouping);
      apiRef.current.forceUpdate();
    }
  }, [apiRef, props.disableRowGrouping]);
  const addRowGroupingCriteria = React.useCallback((field, groupingIndex) => {
    const currentModel = (0, _gridRowGroupingSelector.gridRowGroupingModelSelector)(apiRef);

    if (currentModel.includes(field)) {
      return;
    }

    const cleanGroupingIndex = groupingIndex != null ? groupingIndex : currentModel.length;
    const updatedModel = [...currentModel.slice(0, cleanGroupingIndex), field, ...currentModel.slice(cleanGroupingIndex)];
    apiRef.current.setRowGroupingModel(updatedModel);
  }, [apiRef]);
  const removeRowGroupingCriteria = React.useCallback(field => {
    const currentModel = (0, _gridRowGroupingSelector.gridRowGroupingModelSelector)(apiRef);

    if (!currentModel.includes(field)) {
      return;
    }

    apiRef.current.setRowGroupingModel(currentModel.filter(el => el !== field));
  }, [apiRef]);
  const setRowGroupingCriteriaIndex = React.useCallback((field, targetIndex) => {
    const currentModel = (0, _gridRowGroupingSelector.gridRowGroupingModelSelector)(apiRef);
    const currentTargetIndex = currentModel.indexOf(field);

    if (currentTargetIndex === -1) {
      return;
    }

    const updatedModel = [...currentModel];
    updatedModel.splice(targetIndex, 0, updatedModel.splice(currentTargetIndex, 1)[0]);
    apiRef.current.setRowGroupingModel(updatedModel);
  }, [apiRef]);
  const rowGroupingApi = {
    setRowGroupingModel,
    addRowGroupingCriteria,
    removeRowGroupingCriteria,
    setRowGroupingCriteriaIndex
  };
  (0, _xDataGridPro.useGridApiMethod)(apiRef, rowGroupingApi, 'GridRowGroupingApi');
  /**
   * PRE-PROCESSING
   */

  const addColumnMenuButtons = React.useCallback((initialValue, column) => {
    if (props.disableRowGrouping) {
      return initialValue;
    }

    let menuItems;

    if ((0, _gridRowGroupingUtils.isGroupingColumn)(column.field)) {
      menuItems = /*#__PURE__*/(0, _jsxRuntime.jsx)(_GridRowGroupingColumnMenuItems.GridRowGroupingColumnMenuItems, {});
    } else if (column.groupable) {
      menuItems = /*#__PURE__*/(0, _jsxRuntime.jsx)(_GridRowGroupableColumnMenuItems.GridRowGroupableColumnMenuItems, {});
    } else {
      menuItems = null;
    }

    if (menuItems == null) {
      return initialValue;
    }

    return [...initialValue, /*#__PURE__*/(0, _jsxRuntime.jsx)(Divider, {}), menuItems];
  }, [props.disableRowGrouping]);
  const stateExportPreProcessing = React.useCallback((prevState, context) => {
    var _props$initialState2, _props$initialState2$;

    const rowGroupingModelToExport = (0, _gridRowGroupingSelector.gridRowGroupingModelSelector)(apiRef);
    const shouldExportRowGroupingModel = // Always export if the `exportOnlyDirtyModels` property is activated
    !context.exportOnlyDirtyModels || // Always export if the model is controlled
    props.rowGroupingModel != null || // Always export if the model has been initialized
    ((_props$initialState2 = props.initialState) == null ? void 0 : (_props$initialState2$ = _props$initialState2.rowGrouping) == null ? void 0 : _props$initialState2$.model) != null || // Export if the model is not empty
    Object.keys(rowGroupingModelToExport).length > 0;

    if (!shouldExportRowGroupingModel) {
      return prevState;
    }

    return (0, _extends2.default)({}, prevState, {
      rowGrouping: {
        model: rowGroupingModelToExport
      }
    });
  }, [apiRef, props.rowGroupingModel, (_props$initialState3 = props.initialState) == null ? void 0 : (_props$initialState3$ = _props$initialState3.rowGrouping) == null ? void 0 : _props$initialState3$.model]);
  const stateRestorePreProcessing = React.useCallback((params, context) => {
    var _context$stateToResto;

    if (props.disableRowGrouping) {
      return params;
    }

    const rowGroupingModel = (_context$stateToResto = context.stateToRestore.rowGrouping) == null ? void 0 : _context$stateToResto.model;

    if (rowGroupingModel != null) {
      apiRef.current.setState((0, _gridRowGroupingUtils.mergeStateWithRowGroupingModel)(rowGroupingModel));
    }

    return params;
  }, [apiRef, props.disableRowGrouping]);
  (0, _internals.useGridRegisterPipeProcessor)(apiRef, 'columnMenu', addColumnMenuButtons);
  (0, _internals.useGridRegisterPipeProcessor)(apiRef, 'exportState', stateExportPreProcessing);
  (0, _internals.useGridRegisterPipeProcessor)(apiRef, 'restoreState', stateRestorePreProcessing);
  /**
   * EVENTS
   */

  const handleCellKeyDown = React.useCallback((params, event) => {
    const cellParams = apiRef.current.getCellParams(params.id, params.field);

    if ((0, _gridRowGroupingUtils.isGroupingColumn)(cellParams.field) && event.key === ' ' && !event.shiftKey) {
      var _gridFilteredDescenda;

      event.stopPropagation();
      event.preventDefault();
      const filteredDescendantCount = (_gridFilteredDescenda = (0, _xDataGridPro.gridFilteredDescendantCountLookupSelector)(apiRef)[params.id]) != null ? _gridFilteredDescenda : 0;
      const isOnGroupingCell = props.rowGroupingColumnMode === 'single' || (0, _gridRowGroupingUtils.getRowGroupingFieldFromGroupingCriteria)(params.rowNode.groupingField) === params.field;

      if (!isOnGroupingCell || filteredDescendantCount === 0) {
        return;
      }

      apiRef.current.setRowChildrenExpansion(params.id, !params.rowNode.childrenExpanded);
    }
  }, [apiRef, props.rowGroupingColumnMode]);
  const checkGroupingColumnsModelDiff = React.useCallback(() => {
    const sanitizedRowGroupingModel = (0, _gridRowGroupingSelector.gridRowGroupingSanitizedModelSelector)(apiRef);
    const rulesOnLastRowTreeCreation = apiRef.current.unstable_caches.rowGrouping.rulesOnLastRowTreeCreation;
    const groupingRules = (0, _gridRowGroupingUtils.getGroupingRules)({
      sanitizedRowGroupingModel,
      columnsLookup: (0, _xDataGridPro.gridColumnLookupSelector)(apiRef)
    });

    if (!(0, _gridRowGroupingUtils.areGroupingRulesEqual)(rulesOnLastRowTreeCreation, groupingRules)) {
      apiRef.current.unstable_caches.rowGrouping.rulesOnLastRowTreeCreation = groupingRules;
      apiRef.current.unstable_requestPipeProcessorsApplication('hydrateColumns');
      (0, _gridRowGroupingUtils.setStrategyAvailability)(apiRef, props.disableRowGrouping); // Refresh the row tree creation strategy processing
      // TODO: Add a clean way to re-run a strategy processing without publishing a private event

      if (apiRef.current.unstable_getActiveStrategy('rowTree') === _gridRowGroupingUtils.ROW_GROUPING_STRATEGY) {
        apiRef.current.publishEvent('activeStrategyProcessorChange', 'rowTreeCreation');
      }
    }
  }, [apiRef, props.disableRowGrouping]);
  (0, _xDataGridPro.useGridApiEventHandler)(apiRef, 'cellKeyDown', handleCellKeyDown);
  (0, _xDataGridPro.useGridApiEventHandler)(apiRef, 'columnsChange', checkGroupingColumnsModelDiff);
  (0, _xDataGridPro.useGridApiEventHandler)(apiRef, 'rowGroupingModelChange', checkGroupingColumnsModelDiff);
  /**
   * EFFECTS
   */

  React.useEffect(() => {
    if (props.rowGroupingModel !== undefined) {
      apiRef.current.setRowGroupingModel(props.rowGroupingModel);
    }
  }, [apiRef, props.rowGroupingModel]);
};

exports.useGridRowGrouping = useGridRowGrouping;