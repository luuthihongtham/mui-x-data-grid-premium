"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.useGridExcelExport = void 0;

var React = _interopRequireWildcard(require("react"));

var _xDataGrid = require("@mui/x-data-grid");

var _internals = require("@mui/x-data-grid/internals");

var _excelSerializer = require("./serializer/excelSerializer");

var _components = require("../../../components");

var _jsxRuntime = require("react/jsx-runtime");

function _getRequireWildcardCache(nodeInterop) { if (typeof WeakMap !== "function") return null; var cacheBabelInterop = new WeakMap(); var cacheNodeInterop = new WeakMap(); return (_getRequireWildcardCache = function (nodeInterop) { return nodeInterop ? cacheNodeInterop : cacheBabelInterop; })(nodeInterop); }

function _interopRequireWildcard(obj, nodeInterop) { if (!nodeInterop && obj && obj.__esModule) { return obj; } if (obj === null || typeof obj !== "object" && typeof obj !== "function") { return { default: obj }; } var cache = _getRequireWildcardCache(nodeInterop); if (cache && cache.has(obj)) { return cache.get(obj); } var newObj = {}; var hasPropertyDescriptor = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var key in obj) { if (key !== "default" && Object.prototype.hasOwnProperty.call(obj, key)) { var desc = hasPropertyDescriptor ? Object.getOwnPropertyDescriptor(obj, key) : null; if (desc && (desc.get || desc.set)) { Object.defineProperty(newObj, key, desc); } else { newObj[key] = obj[key]; } } } newObj.default = obj; if (cache) { cache.set(obj, newObj); } return newObj; }

/**
 * @requires useGridColumns (state)
 * @requires useGridFilter (state)
 * @requires useGridSorting (state)
 * @requires useGridSelection (state)
 * @requires useGridParamsApi (method)
 */
const useGridExcelExport = apiRef => {
  const logger = (0, _xDataGrid.useGridLogger)(apiRef, 'useGridExcelExport');
  const getDataAsExcel = React.useCallback((options = {}) => {
    var _options$getRowsToExp, _options$includeHeade, _options$includeColum;

    logger.debug(`Get data as excel`);
    const getRowsToExport = (_options$getRowsToExp = options.getRowsToExport) != null ? _options$getRowsToExp : _internals.defaultGetRowsToExport;
    const exportedRowIds = getRowsToExport({
      apiRef
    });
    const exportedColumns = (0, _internals.getColumnsToExport)({
      apiRef,
      options
    });
    return (0, _excelSerializer.buildExcel)({
      columns: exportedColumns,
      rowIds: exportedRowIds,
      includeHeaders: (_options$includeHeade = options.includeHeaders) != null ? _options$includeHeade : true,
      includeColumnGroupsHeaders: (_options$includeColum = options.includeColumnGroupsHeaders) != null ? _options$includeColum : true,
      valueOptionsSheetName: (options == null ? void 0 : options.valueOptionsSheetName) || 'Options',
      columnsStyles: options == null ? void 0 : options.columnsStyles,
      exceljsPreProcess: options == null ? void 0 : options.exceljsPreProcess,
      exceljsPostProcess: options == null ? void 0 : options.exceljsPostProcess
    }, apiRef.current);
  }, [logger, apiRef]);
  const exportDataAsExcel = React.useCallback(async options => {
    logger.debug(`Export data as excel`);
    const workbook = await getDataAsExcel(options);

    if (workbook === null) {
      return;
    }

    const content = await workbook.xlsx.writeBuffer();
    const blob = new Blob([content], {
      type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    });
    (0, _internals.exportAs)(blob, 'xlsx', options == null ? void 0 : options.fileName);
  }, [logger, getDataAsExcel]);
  const excelExportApi = {
    getDataAsExcel,
    exportDataAsExcel
  };
  (0, _xDataGrid.useGridApiMethod)(apiRef, excelExportApi, 'GridExcelExportApi');
  /**
   * PRE-PROCESSING
   */

  const addExportMenuButtons = React.useCallback((initialValue, options) => {
    var _options$excelOptions;

    if ((_options$excelOptions = options.excelOptions) != null && _options$excelOptions.disableToolbarButton) {
      return initialValue;
    }

    return [...initialValue, {
      component: /*#__PURE__*/(0, _jsxRuntime.jsx)(_components.GridExcelExportMenuItem, {
        options: options.excelOptions
      }),
      componentName: 'excelExport'
    }];
  }, []);
  (0, _internals.useGridRegisterPipeProcessor)(apiRef, 'exportMenu', addExportMenuButtons);
};

exports.useGridExcelExport = useGridExcelExport;