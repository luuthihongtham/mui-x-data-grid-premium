/// <reference types="react" />
import { GridRenderCellParams } from '@mui/x-data-grid-pro';
declare const GridGroupingColumnFooterCell: (props: GridRenderCellParams) => JSX.Element;
export { GridGroupingColumnFooterCell };
