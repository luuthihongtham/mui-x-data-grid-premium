/// <reference types="react" />
import { GridColumnHeaderParams } from '@mui/x-data-grid';
declare const GridAggregationHeader: (props: GridColumnHeaderParams) => JSX.Element | null;
export { GridAggregationHeader };
