import _extends from "@babel/runtime/helpers/esm/extends";
import _objectWithoutPropertiesLoose from "@babel/runtime/helpers/esm/objectWithoutPropertiesLoose";
import _toPropertyKey from "@babel/runtime/helpers/esm/toPropertyKey";
import * as React from 'react';
import { useGridSelector } from '@mui/x-data-grid-pro';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import { unstable_useId as useId } from '@mui/material/utils';
import Select from '@mui/material/Select';
import { useGridApiContext } from '../hooks/utils/useGridApiContext';
import { useGridRootProps } from '../hooks/utils/useGridRootProps';
import { canColumnHaveAggregationFunction, getAggregationFunctionLabel } from '../hooks/features/aggregation/gridAggregationUtils';
import { gridAggregationModelSelector } from '../hooks/features/aggregation/gridAggregationSelectors';
import { jsx as _jsx } from "react/jsx-runtime";
import { jsxs as _jsxs } from "react/jsx-runtime";
export const GridAggregationColumnMenuItem = props => {
  const {
    column,
    label,
    availableAggregationFunctions
  } = props;
  const apiRef = useGridApiContext();
  const rootProps = useGridRootProps();
  const id = useId();
  const aggregationModel = useGridSelector(apiRef, gridAggregationModelSelector);
  const selectedAggregationRule = React.useMemo(() => {
    if (!column || !aggregationModel[column.field]) {
      return '';
    }

    const aggregationFunctionName = aggregationModel[column.field];

    if (canColumnHaveAggregationFunction({
      column,
      aggregationFunctionName,
      aggregationFunction: rootProps.aggregationFunctions[aggregationFunctionName]
    })) {
      return aggregationFunctionName;
    }

    return '';
  }, [rootProps.aggregationFunctions, aggregationModel, column]);

  const handleAggregationItemChange = event => {
    const newAggregationItem = event.target.value || undefined;
    const currentModel = gridAggregationModelSelector(apiRef);

    const _column$field = column.field,
          otherColumnItems = _objectWithoutPropertiesLoose(currentModel, [_column$field].map(_toPropertyKey));

    const newModel = newAggregationItem == null ? otherColumnItems : _extends({}, otherColumnItems, {
      [column.field]: newAggregationItem
    });
    apiRef.current.setAggregationModel(newModel);
    apiRef.current.hideColumnMenu();
  };

  return /*#__PURE__*/_jsx(MenuItem, {
    disableRipple: true,
    children: /*#__PURE__*/_jsxs(FormControl, {
      size: "small",
      sx: {
        width: 150
      },
      children: [/*#__PURE__*/_jsx(InputLabel, {
        id: `${id}-label`,
        children: label
      }), /*#__PURE__*/_jsxs(Select, {
        labelId: `${id}-label`,
        id: `${id}-input`,
        value: selectedAggregationRule,
        label: label,
        onChange: handleAggregationItemChange,
        onBlur: e => e.stopPropagation(),
        fullWidth: true,
        children: [/*#__PURE__*/_jsx(MenuItem, {
          value: "",
          children: "..."
        }), availableAggregationFunctions.map(aggFunc => /*#__PURE__*/_jsx(MenuItem, {
          value: aggFunc,
          children: getAggregationFunctionLabel({
            apiRef,
            aggregationRule: {
              aggregationFunctionName: aggFunc,
              aggregationFunction: rootProps.aggregationFunctions[aggFunc]
            }
          })
        }, aggFunc))]
      })]
    })
  });
};