/// <reference types="react" />
import { GridColDef } from '@mui/x-data-grid-pro';
interface GridAggregationColumnMenuItemsProps {
    column: GridColDef;
    label: string;
    availableAggregationFunctions: string[];
}
export declare const GridAggregationColumnMenuItem: (props: GridAggregationColumnMenuItemsProps) => JSX.Element;
export {};
