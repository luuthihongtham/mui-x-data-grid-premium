/// <reference types="react" />
import { GridRenderCellParams } from '@mui/x-data-grid-pro';
declare const GridGroupingColumnLeafCell: (props: GridRenderCellParams) => JSX.Element;
export { GridGroupingColumnLeafCell };
