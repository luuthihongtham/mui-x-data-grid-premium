/// <reference types="react" />
import { GridExportMenuItemProps } from '@mui/x-data-grid-pro';
import { GridExcelExportOptions } from '../hooks/features/export';
export declare type GridExcelExportMenuItemProps = GridExportMenuItemProps<GridExcelExportOptions>;
declare const GridExcelExportMenuItem: {
    (props: GridExcelExportMenuItemProps): JSX.Element;
    propTypes: any;
};
export { GridExcelExportMenuItem };
