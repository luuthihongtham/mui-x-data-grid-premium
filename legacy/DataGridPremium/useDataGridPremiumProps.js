import _slicedToArray from "@babel/runtime/helpers/esm/slicedToArray";
import _extends from "@babel/runtime/helpers/esm/extends";
import * as React from 'react';
import { useThemeProps } from '@mui/material/styles';
import { DATA_GRID_PRO_PROPS_DEFAULT_VALUES, DATA_GRID_DEFAULT_SLOTS_COMPONENTS, GRID_DEFAULT_LOCALE_TEXT } from '@mui/x-data-grid-pro';
import { GRID_AGGREGATION_FUNCTIONS } from '../hooks/features/aggregation';
/**
 * The default values of `DataGridPremiumPropsWithDefaultValue` to inject in the props of DataGridPremium.
 */

export var DATA_GRID_PREMIUM_PROPS_DEFAULT_VALUES = _extends({}, DATA_GRID_PRO_PROPS_DEFAULT_VALUES, {
  disableAggregation: false,
  disableRowGrouping: false,
  rowGroupingColumnMode: 'single',
  aggregationFunctions: GRID_AGGREGATION_FUNCTIONS,
  aggregationRowsScope: 'filtered',
  getAggregationPosition: function getAggregationPosition(groupNode) {
    return groupNode == null ? 'footer' : 'inline';
  }
});
export var useDataGridPremiumProps = function useDataGridPremiumProps(inProps) {
  var themedProps = useThemeProps({
    props: inProps,
    name: 'MuiDataGrid'
  });
  var localeText = React.useMemo(function () {
    return _extends({}, GRID_DEFAULT_LOCALE_TEXT, themedProps.localeText);
  }, [themedProps.localeText]);
  var components = React.useMemo(function () {
    var overrides = themedProps.components;

    if (!overrides) {
      return _extends({}, DATA_GRID_DEFAULT_SLOTS_COMPONENTS);
    }

    var mergedComponents = {};
    Object.entries(DATA_GRID_DEFAULT_SLOTS_COMPONENTS).forEach(function (_ref) {
      var _ref2 = _slicedToArray(_ref, 2),
          key = _ref2[0],
          defaultComponent = _ref2[1];

      mergedComponents[key] = overrides[key] === undefined ? defaultComponent : overrides[key];
    });
    return mergedComponents;
  }, [themedProps.components]);
  return React.useMemo(function () {
    var _themedProps$experime;

    return _extends({}, DATA_GRID_PREMIUM_PROPS_DEFAULT_VALUES, themedProps, {
      disableAggregation: themedProps.disableAggregation || !((_themedProps$experime = themedProps.experimentalFeatures) != null && _themedProps$experime.aggregation),
      localeText: localeText,
      components: components,
      signature: 'DataGridPremium'
    });
  }, [themedProps, localeText, components]);
};