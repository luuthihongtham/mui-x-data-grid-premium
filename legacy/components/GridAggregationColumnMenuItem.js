import _defineProperty from "@babel/runtime/helpers/esm/defineProperty";
import _extends from "@babel/runtime/helpers/esm/extends";
import _objectWithoutProperties from "@babel/runtime/helpers/esm/objectWithoutProperties";
import _toPropertyKey from "@babel/runtime/helpers/esm/toPropertyKey";
import * as React from 'react';
import { useGridSelector } from '@mui/x-data-grid-pro';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import InputLabel from '@mui/material/InputLabel';
import { unstable_useId as useId } from '@mui/material/utils';
import Select from '@mui/material/Select';
import { useGridApiContext } from '../hooks/utils/useGridApiContext';
import { useGridRootProps } from '../hooks/utils/useGridRootProps';
import { canColumnHaveAggregationFunction, getAggregationFunctionLabel } from '../hooks/features/aggregation/gridAggregationUtils';
import { gridAggregationModelSelector } from '../hooks/features/aggregation/gridAggregationSelectors';
import { jsx as _jsx } from "react/jsx-runtime";
import { jsxs as _jsxs } from "react/jsx-runtime";
export var GridAggregationColumnMenuItem = function GridAggregationColumnMenuItem(props) {
  var column = props.column,
      label = props.label,
      availableAggregationFunctions = props.availableAggregationFunctions;
  var apiRef = useGridApiContext();
  var rootProps = useGridRootProps();
  var id = useId();
  var aggregationModel = useGridSelector(apiRef, gridAggregationModelSelector);
  var selectedAggregationRule = React.useMemo(function () {
    if (!column || !aggregationModel[column.field]) {
      return '';
    }

    var aggregationFunctionName = aggregationModel[column.field];

    if (canColumnHaveAggregationFunction({
      column: column,
      aggregationFunctionName: aggregationFunctionName,
      aggregationFunction: rootProps.aggregationFunctions[aggregationFunctionName]
    })) {
      return aggregationFunctionName;
    }

    return '';
  }, [rootProps.aggregationFunctions, aggregationModel, column]);

  var handleAggregationItemChange = function handleAggregationItemChange(event) {
    var newAggregationItem = event.target.value || undefined;
    var currentModel = gridAggregationModelSelector(apiRef);

    var _column$field = column.field,
        columnItem = currentModel[_column$field],
        otherColumnItems = _objectWithoutProperties(currentModel, [_column$field].map(_toPropertyKey));

    var newModel = newAggregationItem == null ? otherColumnItems : _extends({}, otherColumnItems, _defineProperty({}, column.field, newAggregationItem));
    apiRef.current.setAggregationModel(newModel);
    apiRef.current.hideColumnMenu();
  };

  return /*#__PURE__*/_jsx(MenuItem, {
    disableRipple: true,
    children: /*#__PURE__*/_jsxs(FormControl, {
      size: "small",
      sx: {
        width: 150
      },
      children: [/*#__PURE__*/_jsx(InputLabel, {
        id: "".concat(id, "-label"),
        children: label
      }), /*#__PURE__*/_jsxs(Select, {
        labelId: "".concat(id, "-label"),
        id: "".concat(id, "-input"),
        value: selectedAggregationRule,
        label: label,
        onChange: handleAggregationItemChange,
        onBlur: function onBlur(e) {
          return e.stopPropagation();
        },
        fullWidth: true,
        children: [/*#__PURE__*/_jsx(MenuItem, {
          value: "",
          children: "..."
        }), availableAggregationFunctions.map(function (aggFunc) {
          return /*#__PURE__*/_jsx(MenuItem, {
            value: aggFunc,
            children: getAggregationFunctionLabel({
              apiRef: apiRef,
              aggregationRule: {
                aggregationFunctionName: aggFunc,
                aggregationFunction: rootProps.aggregationFunctions[aggFunc]
              }
            })
          }, aggFunc);
        })]
      })]
    })
  });
};