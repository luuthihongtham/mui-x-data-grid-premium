import _toConsumableArray from "@babel/runtime/helpers/esm/toConsumableArray";
import _extends from "@babel/runtime/helpers/esm/extends";
import _defineProperty from "@babel/runtime/helpers/esm/defineProperty";
import _typeof from "@babel/runtime/helpers/esm/typeof";
import _asyncToGenerator from "@babel/runtime/helpers/esm/asyncToGenerator";

var _defaultColumnsStyles;

import _regeneratorRuntime from "@babel/runtime/regenerator";
import { GRID_DATE_COL_DEF, GRID_DATETIME_COL_DEF } from '@mui/x-data-grid-pro';
import { buildWarning } from '@mui/x-data-grid/internals';

var getExcelJs = /*#__PURE__*/function () {
  var _ref = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime.mark(function _callee() {
    var _yield$import, excelJsDefault;

    return _regeneratorRuntime.wrap(function _callee$(_context) {
      while (1) {
        switch (_context.prev = _context.next) {
          case 0:
            _context.next = 2;
            return import('exceljs');

          case 2:
            _yield$import = _context.sent;
            excelJsDefault = _yield$import.default;
            return _context.abrupt("return", excelJsDefault);

          case 5:
          case "end":
            return _context.stop();
        }
      }
    }, _callee);
  }));

  return function getExcelJs() {
    return _ref.apply(this, arguments);
  };
}();

var warnInvalidFormattedValue = buildWarning(['MUI: When the value of a field is an object or a `renderCell` is provided, the Excel export might not display the value correctly.', 'You can provide a `valueFormatter` with a string representation to be used.']);

var getFormattedValueOptions = function getFormattedValueOptions(colDef, valueOptions, api) {
  if (!colDef.valueOptions) {
    return [];
  }

  var valueOptionsFormatted = valueOptions;

  if (colDef.valueFormatter) {
    valueOptionsFormatted = valueOptionsFormatted.map(function (option) {
      if (_typeof(option) === 'object') {
        return option;
      }

      var params = {
        field: colDef.field,
        api: api,
        value: option
      };
      return String(colDef.valueFormatter(params));
    });
  }

  return valueOptionsFormatted.map(function (option) {
    return _typeof(option) === 'object' ? option.label : option;
  });
};

var serializeRow = function serializeRow(id, columns, api, defaultValueOptionsFormulae) {
  var row = {};
  var dataValidation = {};
  var mergedCells = [];
  var firstCellParams = api.getCellParams(id, columns[0].field);
  var outlineLevel = firstCellParams.rowNode.depth; // `colSpan` is only calculated for rendered rows, so we need to calculate it during export for every row

  api.unstable_calculateColSpan({
    rowId: id,
    minFirstColumn: 0,
    maxLastColumn: columns.length,
    columns: columns
  });
  columns.forEach(function (column, colIndex) {
    var colSpanInfo = api.unstable_getCellColSpanInfo(id, colIndex);

    if (colSpanInfo && colSpanInfo.spannedByColSpan) {
      return;
    }

    if (colSpanInfo && colSpanInfo.cellProps.colSpan > 1) {
      mergedCells.push({
        leftIndex: colIndex + 1,
        rightIndex: colIndex + colSpanInfo.cellProps.colSpan
      });
    }

    var cellParams = api.getCellParams(id, column.field);

    switch (cellParams.colDef.type) {
      case 'singleSelect':
        {
          var _formattedValue$label;

          if (typeof cellParams.colDef.valueOptions === 'function') {
            // If value option depends on the row, set specific options to the cell
            // This dataValidation is buggy with LibreOffice and does not allow to have coma
            var valueOptions = cellParams.colDef.valueOptions({
              id: id,
              row: row,
              field: cellParams.field
            });
            var formattedValueOptions = getFormattedValueOptions(cellParams.colDef, valueOptions, api);
            dataValidation[column.field] = {
              type: 'list',
              allowBlank: true,
              formulae: ["\"".concat(formattedValueOptions.map(function (x) {
                return x.toString().replaceAll(',', 'CHAR(44)');
              }).join(','), "\"")]
            };
          } else {
            // If value option is defined for the column, refer to another sheet
            dataValidation[column.field] = {
              type: 'list',
              allowBlank: true,
              formulae: [defaultValueOptionsFormulae[column.field]]
            };
          }

          var formattedValue = api.getCellParams(id, column.field).formattedValue;

          if (process.env.NODE_ENV !== 'production') {
            if (String(cellParams.formattedValue) === '[object Object]') {
              warnInvalidFormattedValue();
            }
          }

          row[column.field] = (_formattedValue$label = formattedValue == null ? void 0 : formattedValue.label) != null ? _formattedValue$label : formattedValue;
          break;
        }

      case 'boolean':
      case 'number':
        row[column.field] = api.getCellParams(id, column.field).value;
        break;

      case 'date':
      case 'dateTime':
        {
          // Excel does not do any timezone conversion, so we create a date using UTC instead of local timezone
          // Solution from: https://github.com/exceljs/exceljs/issues/486#issuecomment-432557582
          // About Date.UTC(): https://developer.mozilla.org/fr/docs/Web/JavaScript/Reference/Global_Objects/Date/UTC#exemples
          var date = api.getCellParams(id, column.field).value; // value may be `undefined` in auto-generated grouping rows

          if (!date) {
            break;
          }

          var utcDate = new Date(Date.UTC(date.getFullYear(), date.getMonth(), date.getDate(), date.getHours(), date.getMinutes(), date.getSeconds()));
          row[column.field] = utcDate;
          break;
        }

      case 'actions':
        break;

      default:
        row[column.field] = api.getCellParams(id, column.field).formattedValue;

        if (process.env.NODE_ENV !== 'production') {
          if (String(cellParams.formattedValue) === '[object Object]') {
            warnInvalidFormattedValue();
          }
        }

        break;
    }
  });
  return {
    row: row,
    dataValidation: dataValidation,
    outlineLevel: outlineLevel,
    mergedCells: mergedCells
  };
};

var defaultColumnsStyles = (_defaultColumnsStyles = {}, _defineProperty(_defaultColumnsStyles, GRID_DATE_COL_DEF.type, {
  numFmt: 'dd.mm.yyyy'
}), _defineProperty(_defaultColumnsStyles, GRID_DATETIME_COL_DEF.type, {
  numFmt: 'dd.mm.yyyy hh:mm'
}), _defaultColumnsStyles);

var serializeColumn = function serializeColumn(column, columnsStyles) {
  var field = column.field,
      type = column.type;
  return {
    key: field,
    // Excel width must stay between 0 and 255 (https://support.microsoft.com/en-us/office/change-the-column-width-and-row-height-72f5e3cc-994d-43e8-ae58-9774a0905f46)
    // From the example of column width behavior (https://docs.microsoft.com/en-US/office/troubleshoot/excel/determine-column-widths#example-of-column-width-behavior)
    // a value of 10 corresponds to 75px. This is an approximation, because column width depends on the the font-size
    width: Math.min(255, column.width ? column.width / 7.5 : 8.43),
    style: _extends({}, type && (defaultColumnsStyles == null ? void 0 : defaultColumnsStyles[type]), columnsStyles == null ? void 0 : columnsStyles[field])
  };
};

var addColumnGroupingHeaders = function addColumnGroupingHeaders(worksheet, columns, api) {
  var maxDepth = Math.max.apply(Math, _toConsumableArray(columns.map(function (_ref2) {
    var _api$unstable_getColu, _api$unstable_getColu2;

    var field = _ref2.field;
    return (_api$unstable_getColu = (_api$unstable_getColu2 = api.unstable_getColumnGroupPath(field)) == null ? void 0 : _api$unstable_getColu2.length) != null ? _api$unstable_getColu : 0;
  })));

  if (maxDepth === 0) {
    return;
  }

  var columnGroupDetails = api.unstable_getAllGroupDetails();

  var _loop = function _loop(rowIndex) {
    var row = columns.map(function (_ref3) {
      var field = _ref3.field;
      var groupingPath = api.unstable_getColumnGroupPath(field);

      if (groupingPath.length <= rowIndex) {
        return {
          groupId: null,
          parents: groupingPath
        };
      }

      return _extends({}, columnGroupDetails[groupingPath[rowIndex]], {
        parents: groupingPath.slice(0, rowIndex)
      });
    });
    var newRow = worksheet.addRow(row.map(function (group) {
      return group.groupId === null ? null : (group == null ? void 0 : group.headerName) || group.groupId;
    })); // use `rowCount`, since worksheet can have additional rows added in `exceljsPreProcess`

    var lastRowIndex = newRow.worksheet.rowCount;
    var leftIndex = 0;
    var rightIndex = 1;

    var _loop2 = function _loop2() {
      var _row$leftIndex = row[leftIndex],
          leftGroupId = _row$leftIndex.groupId,
          leftParents = _row$leftIndex.parents;
      var _row$rightIndex = row[rightIndex],
          rightGroupId = _row$rightIndex.groupId,
          rightParents = _row$rightIndex.parents;
      var areInSameGroup = leftGroupId === rightGroupId && leftParents.length === rightParents.length && leftParents.every(function (leftParent, index) {
        return rightParents[index] === leftParent;
      });

      if (areInSameGroup) {
        rightIndex += 1;
      } else {
        if (rightIndex - leftIndex > 1) {
          worksheet.mergeCells(lastRowIndex, leftIndex + 1, lastRowIndex, rightIndex);
        }

        leftIndex = rightIndex;
        rightIndex += 1;
      }
    };

    while (rightIndex < columns.length) {
      _loop2();
    }

    if (rightIndex - leftIndex > 1) {
      worksheet.mergeCells(lastRowIndex, leftIndex + 1, lastRowIndex, rightIndex);
    }
  };

  for (var rowIndex = 0; rowIndex < maxDepth; rowIndex += 1) {
    _loop(rowIndex);
  }
};

export function buildExcel(_x, _x2) {
  return _buildExcel.apply(this, arguments);
}

function _buildExcel() {
  _buildExcel = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime.mark(function _callee2(options, api) {
    var columns, rowIds, includeHeaders, includeColumnGroupsHeaders, valueOptionsSheetName, exceljsPreProcess, exceljsPostProcess, _options$columnsStyle, columnsStyles, excelJS, workbook, worksheet, columnsWithArrayValueOptions, defaultValueOptionsFormulae, valueOptionsWorksheet;

    return _regeneratorRuntime.wrap(function _callee2$(_context2) {
      while (1) {
        switch (_context2.prev = _context2.next) {
          case 0:
            columns = options.columns, rowIds = options.rowIds, includeHeaders = options.includeHeaders, includeColumnGroupsHeaders = options.includeColumnGroupsHeaders, valueOptionsSheetName = options.valueOptionsSheetName, exceljsPreProcess = options.exceljsPreProcess, exceljsPostProcess = options.exceljsPostProcess, _options$columnsStyle = options.columnsStyles, columnsStyles = _options$columnsStyle === void 0 ? {} : _options$columnsStyle;
            _context2.next = 3;
            return getExcelJs();

          case 3:
            excelJS = _context2.sent;
            workbook = new excelJS.Workbook();
            worksheet = workbook.addWorksheet('Sheet1');
            worksheet.columns = columns.map(function (column) {
              return serializeColumn(column, columnsStyles);
            });

            if (!exceljsPreProcess) {
              _context2.next = 10;
              break;
            }

            _context2.next = 10;
            return exceljsPreProcess({
              workbook: workbook,
              worksheet: worksheet
            });

          case 10:
            if (includeColumnGroupsHeaders) {
              addColumnGroupingHeaders(worksheet, columns, api);
            }

            if (includeHeaders) {
              worksheet.addRow(columns.map(function (column) {
                return column.headerName || column.field;
              }));
            }

            columnsWithArrayValueOptions = columns.filter(function (column) {
              return column.type === 'singleSelect' && column.valueOptions && typeof column.valueOptions !== 'function';
            });
            defaultValueOptionsFormulae = {};

            if (columnsWithArrayValueOptions.length) {
              valueOptionsWorksheet = workbook.addWorksheet(valueOptionsSheetName);
              valueOptionsWorksheet.columns = columnsWithArrayValueOptions.map(function (_ref4) {
                var field = _ref4.field;
                return {
                  key: field
                };
              });
              columnsWithArrayValueOptions.forEach(function (column) {
                var formattedValueOptions = getFormattedValueOptions(column, column.valueOptions, api);
                valueOptionsWorksheet.getColumn(column.field).values = [column.headerName || column.field].concat(_toConsumableArray(formattedValueOptions));
                var columnLetter = valueOptionsWorksheet.getColumn(column.field).letter;
                defaultValueOptionsFormulae[column.field] = "".concat(valueOptionsSheetName, "!$").concat(columnLetter, "$2:$").concat(columnLetter, "$").concat(1 + formattedValueOptions.length);
              });
            }

            rowIds.forEach(function (id) {
              var _serializeRow = serializeRow(id, columns, api, defaultValueOptionsFormulae),
                  row = _serializeRow.row,
                  dataValidation = _serializeRow.dataValidation,
                  outlineLevel = _serializeRow.outlineLevel,
                  mergedCells = _serializeRow.mergedCells;

              var newRow = worksheet.addRow(row);
              Object.keys(dataValidation).forEach(function (field) {
                newRow.getCell(field).dataValidation = _extends({}, dataValidation[field]);
              });

              if (outlineLevel) {
                newRow.outlineLevel = outlineLevel;
              } // use `rowCount`, since worksheet can have additional rows added in `exceljsPreProcess`


              var lastRowIndex = newRow.worksheet.rowCount;
              mergedCells.forEach(function (mergedCell) {
                worksheet.mergeCells(lastRowIndex, mergedCell.leftIndex, lastRowIndex, mergedCell.rightIndex);
              });
            });

            if (!exceljsPostProcess) {
              _context2.next = 19;
              break;
            }

            _context2.next = 19;
            return exceljsPostProcess({
              workbook: workbook,
              worksheet: worksheet
            });

          case 19:
            return _context2.abrupt("return", workbook);

          case 20:
          case "end":
            return _context2.stop();
        }
      }
    }, _callee2);
  }));
  return _buildExcel.apply(this, arguments);
}