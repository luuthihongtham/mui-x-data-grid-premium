import _toConsumableArray from "@babel/runtime/helpers/esm/toConsumableArray";
import _asyncToGenerator from "@babel/runtime/helpers/esm/asyncToGenerator";
import _regeneratorRuntime from "@babel/runtime/regenerator";
import * as React from 'react';
import { useGridApiMethod, useGridLogger } from '@mui/x-data-grid';
import { useGridRegisterPipeProcessor, exportAs, getColumnsToExport, defaultGetRowsToExport } from '@mui/x-data-grid/internals';
import { buildExcel } from './serializer/excelSerializer';
import { GridExcelExportMenuItem } from '../../../components';
/**
 * @requires useGridColumns (state)
 * @requires useGridFilter (state)
 * @requires useGridSorting (state)
 * @requires useGridSelection (state)
 * @requires useGridParamsApi (method)
 */

import { jsx as _jsx } from "react/jsx-runtime";
export var useGridExcelExport = function useGridExcelExport(apiRef) {
  var logger = useGridLogger(apiRef, 'useGridExcelExport');
  var getDataAsExcel = React.useCallback(function () {
    var _options$getRowsToExp, _options$includeHeade, _options$includeColum;

    var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};
    logger.debug("Get data as excel");
    var getRowsToExport = (_options$getRowsToExp = options.getRowsToExport) != null ? _options$getRowsToExp : defaultGetRowsToExport;
    var exportedRowIds = getRowsToExport({
      apiRef: apiRef
    });
    var exportedColumns = getColumnsToExport({
      apiRef: apiRef,
      options: options
    });
    return buildExcel({
      columns: exportedColumns,
      rowIds: exportedRowIds,
      includeHeaders: (_options$includeHeade = options.includeHeaders) != null ? _options$includeHeade : true,
      includeColumnGroupsHeaders: (_options$includeColum = options.includeColumnGroupsHeaders) != null ? _options$includeColum : true,
      valueOptionsSheetName: (options == null ? void 0 : options.valueOptionsSheetName) || 'Options',
      columnsStyles: options == null ? void 0 : options.columnsStyles,
      exceljsPreProcess: options == null ? void 0 : options.exceljsPreProcess,
      exceljsPostProcess: options == null ? void 0 : options.exceljsPostProcess
    }, apiRef.current);
  }, [logger, apiRef]);
  var exportDataAsExcel = React.useCallback( /*#__PURE__*/function () {
    var _ref = _asyncToGenerator( /*#__PURE__*/_regeneratorRuntime.mark(function _callee(options) {
      var workbook, content, blob;
      return _regeneratorRuntime.wrap(function _callee$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              logger.debug("Export data as excel");
              _context.next = 3;
              return getDataAsExcel(options);

            case 3:
              workbook = _context.sent;

              if (!(workbook === null)) {
                _context.next = 6;
                break;
              }

              return _context.abrupt("return");

            case 6:
              _context.next = 8;
              return workbook.xlsx.writeBuffer();

            case 8:
              content = _context.sent;
              blob = new Blob([content], {
                type: 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
              });
              exportAs(blob, 'xlsx', options == null ? void 0 : options.fileName);

            case 11:
            case "end":
              return _context.stop();
          }
        }
      }, _callee);
    }));

    return function (_x) {
      return _ref.apply(this, arguments);
    };
  }(), [logger, getDataAsExcel]);
  var excelExportApi = {
    getDataAsExcel: getDataAsExcel,
    exportDataAsExcel: exportDataAsExcel
  };
  useGridApiMethod(apiRef, excelExportApi, 'GridExcelExportApi');
  /**
   * PRE-PROCESSING
   */

  var addExportMenuButtons = React.useCallback(function (initialValue, options) {
    var _options$excelOptions;

    if ((_options$excelOptions = options.excelOptions) != null && _options$excelOptions.disableToolbarButton) {
      return initialValue;
    }

    return [].concat(_toConsumableArray(initialValue), [{
      component: /*#__PURE__*/_jsx(GridExcelExportMenuItem, {
        options: options.excelOptions
      }),
      componentName: 'excelExport'
    }]);
  }, []);
  useGridRegisterPipeProcessor(apiRef, 'exportMenu', addExportMenuButtons);
};