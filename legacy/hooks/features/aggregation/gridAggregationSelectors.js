import { createSelector } from '@mui/x-data-grid-pro/internals';
export var gridAggregationStateSelector = function gridAggregationStateSelector(state) {
  return state.aggregation;
};
export var gridAggregationModelSelector = createSelector(gridAggregationStateSelector, function (aggregationState) {
  return aggregationState.model;
});
export var gridAggregationLookupSelector = createSelector(gridAggregationStateSelector, function (aggregationState) {
  return aggregationState.lookup;
});