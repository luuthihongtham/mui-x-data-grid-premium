import _extends from "@babel/runtime/helpers/esm/extends";
import _toConsumableArray from "@babel/runtime/helpers/esm/toConsumableArray";
import * as React from 'react';
import MuiDivider from '@mui/material/Divider';
import { gridColumnLookupSelector } from '@mui/x-data-grid-pro';
import { useGridRegisterPipeProcessor } from '@mui/x-data-grid-pro/internals';
import { getAvailableAggregationFunctions, addFooterRows, getAggregationRules, mergeStateWithAggregationModel } from './gridAggregationUtils';
import { wrapColumnWithAggregationValue, unwrapColumnFromAggregation } from './wrapColumnWithAggregation';
import { GridAggregationColumnMenuItem } from '../../../components/GridAggregationColumnMenuItem';
import { gridAggregationModelSelector } from './gridAggregationSelectors';
import { jsx as _jsx } from "react/jsx-runtime";

var Divider = function Divider() {
  return /*#__PURE__*/_jsx(MuiDivider, {
    onClick: function onClick(event) {
      return event.stopPropagation();
    }
  });
};

export var useGridAggregationPreProcessors = function useGridAggregationPreProcessors(apiRef, props) {
  var updateAggregatedColumns = React.useCallback(function (columnsState) {
    var rulesOnLastColumnHydration = apiRef.current.unstable_caches.aggregation.rulesOnLastColumnHydration;
    var aggregationRules = props.disableAggregation ? {} : getAggregationRules({
      columnsLookup: columnsState.lookup,
      aggregationModel: gridAggregationModelSelector(apiRef),
      aggregationFunctions: props.aggregationFunctions
    });
    columnsState.all.forEach(function (field) {
      var shouldHaveAggregationValue = !!aggregationRules[field];
      var haveAggregationColumnValue = !!rulesOnLastColumnHydration[field];
      var column = columnsState.lookup[field];

      if (haveAggregationColumnValue) {
        column = unwrapColumnFromAggregation({
          column: column
        });
      }

      if (shouldHaveAggregationValue) {
        column = wrapColumnWithAggregationValue({
          column: column,
          aggregationRule: aggregationRules[field],
          apiRef: apiRef
        });
      }

      columnsState.lookup[field] = column;
    });
    apiRef.current.unstable_caches.aggregation.rulesOnLastColumnHydration = aggregationRules;
    return columnsState;
  }, [apiRef, props.aggregationFunctions, props.disableAggregation]);
  var addGroupFooterRows = React.useCallback(function (groupingParams) {
    var newGroupingParams;
    var rulesOnLastRowHydration;

    if (props.disableAggregation) {
      newGroupingParams = groupingParams;
      rulesOnLastRowHydration = {};
    } else {
      var aggregationRules = getAggregationRules({
        columnsLookup: gridColumnLookupSelector(apiRef),
        aggregationModel: gridAggregationModelSelector(apiRef),
        aggregationFunctions: props.aggregationFunctions
      });
      rulesOnLastRowHydration = aggregationRules; // If no column have an aggregation rule
      // Then don't create the footer rows

      if (Object.values(aggregationRules).length === 0) {
        newGroupingParams = groupingParams;
      } else {
        newGroupingParams = addFooterRows({
          groupingParams: groupingParams,
          aggregationRules: aggregationRules,
          getAggregationPosition: props.getAggregationPosition,
          apiRef: apiRef
        });
      }
    }

    apiRef.current.unstable_caches.aggregation.rulesOnLastRowHydration = rulesOnLastRowHydration;
    return newGroupingParams;
  }, [apiRef, props.disableAggregation, props.getAggregationPosition, props.aggregationFunctions]);
  var addColumnMenuButtons = React.useCallback(function (initialValue, column) {
    if (props.disableAggregation) {
      return initialValue;
    }

    var availableAggregationFunctions = getAvailableAggregationFunctions({
      aggregationFunctions: props.aggregationFunctions,
      column: column
    });

    if (availableAggregationFunctions.length === 0) {
      return initialValue;
    }

    return [].concat(_toConsumableArray(initialValue), [/*#__PURE__*/_jsx(Divider, {}), /*#__PURE__*/_jsx(GridAggregationColumnMenuItem, {
      column: column,
      label: apiRef.current.getLocaleText('aggregationMenuItemHeader'),
      availableAggregationFunctions: availableAggregationFunctions
    })]);
  }, [apiRef, props.aggregationFunctions, props.disableAggregation]);
  var stateExportPreProcessing = React.useCallback(function (prevState) {
    if (props.disableAggregation) {
      return prevState;
    }

    var aggregationModelToExport = gridAggregationModelSelector(apiRef);

    if (Object.values(aggregationModelToExport).length === 0) {
      return prevState;
    }

    return _extends({}, prevState, {
      aggregation: {
        model: aggregationModelToExport
      }
    });
  }, [apiRef, props.disableAggregation]);
  var stateRestorePreProcessing = React.useCallback(function (params, context) {
    var _context$stateToResto;

    if (props.disableAggregation) {
      return params;
    }

    var aggregationModel = (_context$stateToResto = context.stateToRestore.aggregation) == null ? void 0 : _context$stateToResto.model;

    if (aggregationModel != null) {
      apiRef.current.setState(mergeStateWithAggregationModel(aggregationModel));
    }

    return params;
  }, [apiRef, props.disableAggregation]);
  useGridRegisterPipeProcessor(apiRef, 'hydrateColumns', updateAggregatedColumns);
  useGridRegisterPipeProcessor(apiRef, 'hydrateRows', addGroupFooterRows);
  useGridRegisterPipeProcessor(apiRef, 'columnMenu', addColumnMenuButtons);
  useGridRegisterPipeProcessor(apiRef, 'exportState', stateExportPreProcessing);
  useGridRegisterPipeProcessor(apiRef, 'restoreState', stateRestorePreProcessing);
};