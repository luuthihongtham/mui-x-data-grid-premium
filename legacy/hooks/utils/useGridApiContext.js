import { useGridApiContext as useCommunityGridApiContext } from '@mui/x-data-grid-pro';
export var useGridApiContext = useCommunityGridApiContext;