import { useGridApiRef as useCommunityGridApiRef } from '@mui/x-data-grid-pro';
export var useGridApiRef = useCommunityGridApiRef;