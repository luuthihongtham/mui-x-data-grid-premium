import { useGridApiContext as useCommunityGridApiContext } from '@mui/x-data-grid-pro';
export const useGridApiContext = useCommunityGridApiContext;