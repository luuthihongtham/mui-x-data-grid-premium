import { createSelector } from '@mui/x-data-grid-pro/internals';
export const gridAggregationStateSelector = state => state.aggregation;
export const gridAggregationModelSelector = createSelector(gridAggregationStateSelector, aggregationState => aggregationState.model);
export const gridAggregationLookupSelector = createSelector(gridAggregationStateSelector, aggregationState => aggregationState.lookup);